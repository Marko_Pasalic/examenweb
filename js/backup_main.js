/**
 * Dropdown countries in registration.html
 */
let countriesSelector = document.querySelector("#countries");
if (typeof (countriesSelector) != 'undefined' && countriesSelector != null) {
    countriesSelector.addEventListener("click", function (e) {
        e.preventDefault();

        fetch("index.php?view=api/countries/countriesList", {
            method: "GET",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }).then(function (response) {
            return response.text();
        }).then(function (response) {
            countriesSelector.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
    });
}

/**
 * Users Role Update from Admin management
 * @param {*} e 
 */
function roleSelector(e) {
    let td = e.closest('td');
    let userid = td.parentNode.firstChild;

    fetch("index.php?view=api/role/rolesList", {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}

function getSelectedOption(e) {
    selected = e.options[e.selectedIndex].value;
    console.log(selected);
    console.log(e.value);

    updateUserRole(e);
}

function updateUserRole(e) {

    let td = e.closest('td');
    let userid = td.parentNode.firstChild;
    fetch("index.php?view=api/user/updateByRoleName/" + userid.textContent + "/" + e.value), {
    }
}

/* Include admin menu */

const includeMenu = document.querySelector(".include-admin-menu");
if (typeof (includeMenu) != 'undefined' && includeMenu != null) {
    fetch("view/admin/menu.html")
        .then(function (response) {
            return response.text();
        }).then(function (response) {
            includeMenu.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
}

/* userModal for Admin */

let callerid = document.querySelector("#m-uid");
let callertoken = document.querySelector("#m-tokenu");
let modalBody = document.querySelector("#modal-user-list-body");
let userModalLinks = document.querySelectorAll(".user-modal-link");
userModalLinks.forEach((link) => {
    if (typeof (link) != 'undefined' && link != null) {
        link.addEventListener("click", function (e) {
            e.preventDefault();
            let td = link.closest('td');
            let userid = td.previousSibling;
            modalBody.innerHTML = '';
            fetch("index.php?view=api/user/getUserModalBody/" + userid.textContent + "/" + callerid.textContent + "/" + callertoken.textContent, {
            }).then(function (response) {
                return response.text();
            }).then(function (response) {
                modalBody.innerHTML = response.toString();
            }).catch((error) => {
                console.log(error.toString());
            });
        });
    }
});

/**
 ** Groups Management
 **/


function createGroup(e) {
    location.href = "index.php?view=view/admin/createGroup"
}

function updateGroup(e) {
    let td = e.closest('td');
    let groupid = td.parentNode.firstChild;
    location.href = "index.php?view=api/groups/updateform/" + groupid.textContent
}

function deleteGroup(e) {
    let td = e.closest('td');
    let groupid = td.parentNode.firstChild;
    if (confirm("Are you sure you want to delete")) {
        location.href = "index.php?view=api/groups/delete/" + groupid.textContent
    }
}

function assignGroupBtn(e){
    location.href = "index.php?view=view/admin/assignGroup"
}

function unassignFormationForm(e){
    location.href = "index.php?view=api/groups/unassignFormationForm"
}

function addUserBtn(e){
    let td = e.closest('td');
    let groupid = td.parentNode.firstChild;
    location.href = "index.php?view=api/groups/addUserForm/" + groupid.textContent
}

function removeUserBtn(e) {
    let td = e.closest('td');
    let groupid = td.parentNode.firstChild;
    location.href = "index.php?view=api/groups/removeUserForm/" + groupid.textContent
}

function getUsernameEnrol(e){
    let groupid = document.getElementById("remove-groupid").value;
    console.log(groupid);
    fetch("index.php?view=api/groups/getUserEnrolForDelete/" + groupid, {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        console.log(e);
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}

function UserGroupList(e){
        fetch("index.php?view=api/groups/getUserEnrol/" + groupid, {
        }).then(function (response) {
            return response.text();
        }).then(function (response) {
            console.log(e);
            e.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
}

/**
 * Get users name from students and teachers
 * @param {*} e 
 */
function userNameForGroup(e) {

    fetch("index.php?view=api/user/getStudentTeacherName", {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}


function groupName(e) {
    console.log(e);
    fetch("index.php?view=api/groups/getAllName", {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}

/**
 * Fetch to unassign group from formation
 */
let formationGroupSelector = document.querySelector("#formation-name");
if (typeof (formationGroupSelector) != 'undefined' && formationGroupSelector != null) {
    formationGroupSelector.addEventListener("change", function (e) {
        e.preventDefault();
        fetch("index.php?view=api/groups/getByFormationForForm/" + formationGroupSelector.value, {
            method: "GET",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }).then(function (response) {
            return response.text();
        }).then(function (response) {
            let groupList = document.querySelector("#group-name");
            groupList.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
    });
}

/**
 * userGroupListModal
 */
let groupcallerid = document.querySelector("#m-uid");
let groupcallertoken = document.querySelector("#m-tokenu");
let groupmodalBody = document.querySelector("#modal-group-user-list");
let groupusermodallink = document.querySelectorAll(".group-user-modal-link");
groupusermodallink.forEach((link) => {
    if (typeof (link) != 'undefined' && link != null) {
        link.addEventListener("click", function (e) {
            e.preventDefault();
            let td = link.closest('td');
            let groupid = td.parentNode.firstChild;
            groupmodalBody.innerHTML = '';
            fetch("index.php?view=api/groups/groupUserListModal/" + groupid.textContent + "/" + groupcallerid.textContent + "/" + groupcallertoken.textContent, {
            }).then(function (response) {
                return response.text();
            }).then(function (response) {
                groupmodalBody.innerHTML = response.toString();
            }).catch((error) => {
                console.log(error.toString());
            });
        });
    }
});

/**
 * Create formation from admin management
 * @param {*} e 
 */
function createFormation(e) {
    location.href = "index.php?view=view/admin/createformation"
}

function updateFormation(e) {
    let td = e.closest('td');
    let formationid = td.parentNode.firstChild;
    location.href = "index.php?view=api/formation/updateform/" + formationid.textContent
}

function deleteFormation(e) {
    let td = e.closest('td');
    let formationid = td.parentNode.firstChild;
    if (confirm("Are you sure you want to delete")) {
        location.href = "index.php?view=api/formation/delete/" + formationid.textContent
    }
}

function FormationsName(e) {
    fetch("index.php?view=api/formation/getAllName", {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}
/**
 * Formation update modal
 */

// let formationcallerid = document.querySelector("#m-uid");
// let formationcallertoken = document.querySelector("#m-tokenu");
// let formationModalBody = document.querySelector("#modal-formation-update-body");
// let formationModalLink = document.querySelectorAll(".formation-modal-link");
// formationModalLink.forEach((link) => {
//     if (typeof (link) != 'undefined' && link != null) {
//         link.addEventListener("click", function (e) {
//             e.preventDefault();
//             let td = link.closest('td');
//             let formationid = td.parentNode.firstChild;
//             formationModalBody.innerHTML = '';
//             fetch("index.php?view=api/formation/getFormationUpdateForm/" + formationid.textContent + "/" + formationcallerid.textContent + "/" + formationcallertoken.textContent, {
//             }).then(function (response) {
//                 return response.text();
//             }).then(function (response) {
//                 formationModalBody.innerHTML = response.toString();
//             }).catch((error) => {
//                 console.log(error.toString());
//             });
//         });
//     }
// });

    function assignCourseBtn(e) {
        location.href = "index.php?view=view/admin/assignCourse"
    }

    function unassignCourseBtn(e){
        location.href = "index.php?view=api/course/unassignCourse"
    }

    function getAllTeacher(e){
        fetch("index.php?view=api/user/getTeacher", {
        }).then(function (response) {
            return response.text();
        }).then(function (response) {
            e.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
    }

/**
 * Create course for admin management
 * @param {*} e 
 */
function createCourse(e) {
    location.href = "index.php?view=api/course/createCourse"
}

function updateCourse(e) {
    let td = e.closest('td');
    let courseid = td.parentNode.firstChild;
    location.href = "index.php?view=api/course/updateform/" + courseid.textContent
}

function deleteCourse(e) {
    let td = e.closest('td');
    let courseid = td.parentNode.firstChild;
    if (confirm("Are you sure you want to delete")) {

        location.href = "index.php?view=api/course/delete/" + courseid.textContent
    }
}

function CoursesName(e) {

    fetch("index.php?view=api/course/getAllName", {
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        e.innerHTML = response.toString();
    }).catch((error) => {
        console.log(error.toString());
    });
}
/**
 * fetch for unassign course form in admin management
 */
let formationSelector = document.querySelector("#formation-name");
if (typeof (formationSelector) != 'undefined' && formationSelector != null) {
    formationSelector.addEventListener("change", function (e) {
        e.preventDefault();
        fetch("index.php?view=api/course/getByFormationForForm/" + formationSelector.value, {
            method: "GET",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }).then(function (response) {
            return response.text();
        }).then(function (response) {
            let courseList = document.querySelector("#course-name");
            courseList.innerHTML = response.toString();
        }).catch((error) => {
            console.log(error.toString());
        });
    });
}

/**
 * Data table
 */
let dt = $('.table-dt, .table-dten');

console.log(dt);

dt.dataTable({
    "pageLength": 25
});

let tabledts = document.querySelectorAll('.table-dt, .table-dten');
console.log(tabledts);

let tabledt = document.querySelector('.table-dt, .table-dten');

$('.table-dtfr').DataTable({
    language: {
        processing: "Traitement en cours...",
        search: "Rechercher&nbsp;:",
        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix: "",
        loadingRecords: "Chargement en cours...",
        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable: "Aucune donnée disponible dans le tableau",
        paginate: {
            first: "Premier",
            previous: "Pr&eacute;c&eacute;dent",
            next: "Suivant",
            last: "Dernier"
        },
        aria: {
            sortAscending: ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    },
    "pageLength": 25
});


/**
 * #button-courses-list
 */
let btncl = document.querySelector('#button-courses-list');

if (btncl) {
    btncl.addEventListener('click', event => {

        let courseslist = document.querySelector('#courses-list').classList;
        if (courseslist.contains('bgblue')) {
            courseslist.replace('bgblue', 'bgyellow');
        } else {
            courseslist.remove('bgyellow');
            courseslist.add('bgblue');
        }
    });
}