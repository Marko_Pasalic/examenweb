<?php

const DB_HOST = 'localhost';
const DB_NAME = 'examenweb';
const DB_USER = 'root';
const DB_PASSWORD = 'root';
const HMAC_SALT = 'test1234';
const JWT_KEY = 'test1234';
const ROOT_PATH = __DIR__;

// alternative syntax of constant declaration
define('ALTERNATIVE_CONST', 'test');
