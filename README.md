# Installation
Vous pouvez retrouver tout le projet sur le gitlab public à cette adresse:
    https://gitlab.com/Marko_Pasalic/examenweb

Il vous sera alors possible d'utiliser le "clone" pour récupérer le projet sur votre machine locale

## Prérequis

* Php 8.1+
* MySQL 5.7+
* Composer

## Fichier de configuration

Le fichier config-dist.php doit être copié et renommé en config.php

Les paramètres de connexion à la DB doivent être indiqués dans ce nouveau fichier.

## Migrations

Une fois la base de données et le fichier de configuration créés, il est nécessaire d'exécuter les migrations DB

Pour ce faire, vous disposez du script migration.php à exécuter en commande CLI

Le script possède aussi une commande "migration --all". Attention, il est possible que celle-ci ne fonctionne pas correctement et présente des erreurs. Il est conseillé de vérifier l'intégrité de la DB auquel cas, il faut migrer les fichiers sql un à un.

## Administrateur

Un compte administrateur est fournis lors de la migration "user".
L'identifiant ainsi que le mot de passe sont respectivement "ROOT" / "ROOT"
!! Attention, ils sont case sensitive. Veillez à utiliser les majuscules.

Il est vivement conseillé de changer votre mot de passe rapidement. Pour ce faire, vous pouvez utiliser le menu "profile"
et mettre à jour les données selon votre convenance.

## Fichier Javascript

Le fichier javascript utilisé pour le projet a été "minifé". Celui-ci se trouve dans le dossier JS/main.js
Un fichier backup_main.js est ajouté en plus. Celui-ci n'est pas fonctionnel mais permet d'avoir une version lisible de ce que contient le fichier main.js

## Php documentation
Il est possible de retrouver la document php dans le dossier ".phpdoc/

## bug connu

- Il peut y avoir un problème dans l'affichage du message de confirmation lorsqu'un admin change le rôle d'un utilisateur.
Le changement est bien actif quant à lui.

- A l'ouverture d'une modal utilisateur, la barre de navigation est injectée aussi.

- La version française du site présente une erreur de dépreciation de la fonction "dateTime" depuis la page de profile.

- Le projet ayant été dévelopé sur MacOs, le système de routing a dû être utilisé autrement forçant le site à passer par "index". 

## To do list

- Gestion de la validation de l'inscription d'un utilisateur à un cours par l'administrateur.
    L'objectif ici est de remplacer l'action "enrol" du bouton présent dans la "course list" par un bouton "en attente"
    et un message d'annonce pour prévenir l'utilisateur que son inscription est prise en compte et sera validée par l'admin.

    En ajoutant un attribut "iswaiting" dans la DB user qui prendra 0 ou 1. Lorsque l'administrateur validera la demande,
    l'action mettra automatiquement la DB à jour et la fonction "enrol" pourra s'activer pour l'utilisateur en question.

    Un système de notification sera ajouté dans une prochaine mise à jour. Depuis le backoffice administrateur, dans la liste des utilisateurs, une alerte pourra être ajoutée au nom d'un utilisateur en attente de validation.
    Nous utiliserons les "badges" bootstrap comme celui-ci par exemple "<span class="badge badge-light">4</span>".

- L'ajout d'une modal pour la partie "formation" de la vue administrateur. Celle-ci fonctionnera exactement comme        celle présente dans le menu de gestion des groupes. Un clique sur le nom de la formation permettra de voir la liste des cours qui sont présents à l'intérieur.

-   Possibilité pour un utilisateur de supprimer son compte. Les données personnelles seront supprimées de la DB mais les informations non sensibles seront conservées à des fins de statistiques. L'utilisateur sera alors mis en "off".
