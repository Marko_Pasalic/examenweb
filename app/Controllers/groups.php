<?php

namespace app\Controllers;

use app\Helpers\Access;
use app\Helpers\Output;
use app\Helpers\Text;

class Groups extends Controller
{


    public function createGroup(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();

        if (
            !empty($_POST['name'])
        ) {


            $group_data = [
                'name' => (htmlentities($_POST['name'], ENT_QUOTES)),
            ];

            if (!$this->model->getByGroupName($group_data['name'])) {
                $groupid = $this->model->createGroup($group_data);
                if ($groupid) {
                    Output::createAlert(Text::getString(['The group was successfully added', 'Le groupe a été ajoutée avec succès']), 'success', 'index.php?view=api/groups/list');
                } else {
                    Output::render('messageBox', Text::getString(['Failed to create group', 'La création du groupe à échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['A group with this name already exists', 'Un groupe avec ce nom existe déjà']));
            }
        } else {
            header('Location: index.php?view=view/admin/createGroup');
            die;
        }
    }

    public function update(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();
        $update = false;

        $group = $this->get($_POST['id']);

        if (!empty($_POST['update-name'])) {
            $group->name = htmlentities($_POST['name'], ENT_QUOTES);
            $update = true;
        }
        $update = true;

        if ($update && $this->model->update($group)) {
            Output::createAlert(Text::getString(['Group update completed successfully', 'La mise à jour du groupe a été effectuée avec succès']), 'success', 'index.php?view=api/groups/list');
        } else {
            Output::createAlert(Text::getString(['Group update failed', 'La mise à jour du groupe a échoué']), 'danger', 'index.php?view=api/groups/list');
        }
    }

    /**
     * Summary of addUser
     * @return void
     */
    public function addUser(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();

        if (!empty($_POST['add-user'])) {
            $user = new User();

            $group_data = [
                'userid' => $user->getUserIdByName($_POST['add-user']),
                'groupid' => $_POST['add-groupid'],

            ];

            if (!$this->model->hasUser($group_data['groupid'], $group_data['userid'])) {
                $groupAddUser = $this->model->AddUser($group_data);
                if ($groupAddUser) {
                    Output::createAlert(Text::getString(['user added successfully', 'l\'utilisateur a été ajouté avec succès']), 'success', 'index.php?view=api/groups/list');
                } else {
                    Output::render('messageBox', Text::getString(['User could not be added', 'L\'utilisateur n\'a pas pu être ajouté']));
                }
            }
            Output::render('messageBox', Text::getString(['This user is already part of this group', 'Cet utilisateur fait déjà partie de ce groupe']));
        }
    }

    /**
     * Summary of removeUser
     * @return void
     */
    public function removeUser(): void
    {
        Access::checkAdmin();

        if (!empty($_POST['remove-user'])) {
            $user = new User();

            $group_data = [
                'groupid' => $_POST['remove-groupid'],
                'userid' => $user->getUserIdByName($_POST['remove-user']),
            ];

            if ($this->model->hasUser($group_data['groupid'], $group_data['userid'])) {
                $groupRemoveUser = $this->model->removeUser($group_data);
                if ($groupRemoveUser) {
                    Output::createAlert(Text::getString(['user has been successfully removed', 'l\'utilisateur a été retirer avec succès']), 'success', 'index.php?view=api/groups/list');
                } else {
                    Output::render('messageBox', Text::getString(['User could not be removed', 'L\'utilisateur n\'a pas pu être retirer']));
                }
            }
            Output::render('messageBox', Text::getString(['This user is not part of this group', 'Cet utilisateur ne fait pas partie de ce groupe']));
        }
    }

    /**
     * Summary of assignFormation
     * @return void
     */
    public function assignFormation(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();


        if (
            !empty($_POST['formation-name'])
            && !empty($_POST['group-name'])
        ) {

            $formation = new Formation();

            $group_formation_data = [
                'groupid' => $this->model->getUserGroupIdByName($_POST['group-name']),
                'formationid' => $formation->getFormationIdByName($_POST['formation-name']),

            ];
            if (!$this->model->hasAssignFormation($group_formation_data['formationid'], $group_formation_data['groupid'])) {
                $group_formationAssign = $this->model->AssignFormation($group_formation_data);
                if ($group_formationAssign) {
                    Output::createAlert(Text::getString(['The group has been successfully assigned', 'Le groupe a été assigné avec succès']), 'success', 'index.php?view=api/groups/list');
                } else {
                    Output::render('messageBox', Text::getString(['The procedure has failed', 'La procédure a échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['This group is already assigned to this formation', 'Ce groupe est déjà assigné à cette formation']));
            }
        } else {
            header('Location: index.php?view=api/groups/list');
            die;
        }
    }

    /**
     * Summary of unassignGroup
     * @return void
     */
    public function unassignGroup(): void
    {
        Access::checkAdmin();

        if (
            !empty($_POST['formation-name'])
            && !empty($_POST['group-name'])
        ) {
            $formation = new Formation();
            $group_data = [
                'formationid' => $_POST['formation-name'],
                'user_groupid' => $this->model->getUserGroupIdByGroupId($_POST['group-name']),
            ];

            if ($formation->model->hasAssignGroup($group_data['formationid'], $group_data['user_groupid'])) {
                $groupUnassign = $this->model->unassignFormation($group_data);
                if ($groupUnassign) {
                    Output::createAlert(Text::getString(['Group has been successfully unassigned from formation', 'Le groupe a été désassigné de la formation avec succès']), 'success', 'index.php?view=api/groups/list');
                } else {
                    Output::render('messageBox', Text::getString(['Group could not be removed from formation', 'Le groupe n\'a pas pu être retirer de la formation']));
                }
            }
            Output::render('messageBox', Text::getString(['The group is not assigned to this formation', 'Le groupe n\est pas assigné à cette formation']));
        }
    }

    /**
     * Summary of delete
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        Access::checkAdmin();

        if ($this->model->delete($id)) {
            Output::createAlert(Text::getString(['The deletion of the group was successful', 'La suppression du groupe a été effectuée avec succès']), 'success', 'index.php?view=api/groups/list');
        } else {
            Output::createAlert(Text::getString(['The deletion of the group has failed', 'La suppression du groupe a échouée']), 'danger', 'index.php?view=api/groups/list');
        }
    }

    /**
     * Summary of getAllName
     * @return void
     */
    public function getAllName(): void
    {
        $groupName = $this->model->getAllName();
        Output::render('genericDropdownModal', $groupName);
    }

    /**
     * Summary of getUserEnrol
     * @param int $groupid
     * @return array
     */
    public function getUserEnrol(int $groupid): array
    {
        return $this->model->getUserEnrol($groupid);
    }

    /**
     * Summary of getUserEnrolForDelete
     * @param int $groupid
     * @return void
     */
    public function getUserEnrolForDelete(int $groupid): void
    {
        $username = $this->model->getUserEnrolName($groupid);
        Output::render('genericDropdownModal', $username);
    }

    /**
     * Summary of getByFormationForForm
     * @param int $formationid
     * @return void
     */
    public function getByFormationForForm(int $formationid): void
    {
        $data = $this->model->getByFormationForForm($formationid);
        Output::render('getFormOptions', $data);
    }

    /**
     * Summary of unassignFormationForm
     * @return void
     */
    public function unassignFormationForm(): void
    {
        Access::checkAdmin();

        $formation = new Formation();
        $data = $formation->model->getAllForForm('name');
        Output::render('unassignGroup', $data);
    }

    /**
     * Summary of updateForm
     * @param int $id
     * @return void
     */
    public function updateForm(int $id): void
    {
        Access::checkAdmin();

        $groupid = $this->get($id);

        Output::render('groupUpdate', $groupid);
    }

    /**
     * Summary of addUserForm
     * @param int $id
     * @return void
     */
    public function addUserForm(int $id): void
    {
        Access::checkAdmin();

        $groupid = $this->get($id);

        Output::render('groupAddUser', $groupid);
    }

    /**
     * Summary of removeUserForm
     * @param int $id
     * @return void
     */
    public function removeUserForm(int $id): void
    {
        Access::checkAdmin();

        $groupid = $this->get($id);

        Output::render('groupUserRemove', $groupid);
    }

    /**
     * Summary of list
     * @return void
     */
    public function list(): void
    {
        Access::checkAdmin();
        $group = $this->model->getAll();

        Output::render('groups', $group);
    }

    /**
     * Summary of groupUserListModal
     * @param int $id
     * @param int $callerid
     * @param string $token
     * @return void
     */
    public function groupUserListModal(int $id, int $callerid, string $token): void
    {
        if (!Access::checkToken($token, 'modal-user-token', $callerid)) {
            header('HTTP/1.1 403');
            die;
        }

        $data = new \stdClass();
        $data->userslist = $this->groupUserLIST($id);

        Output::render('groupUserListModal', $data);
    }

    /** */
    public function groupUserLIST(int $id, bool $return = true): mixed
    {
        $groups = new Groups();
        $groups = $groups->getUserEnrol($id);
        if ($return) {
            return Output::get('groupUserList', $groups);
        } else {
            echo Output::get('groupUserList', $groups);
        }
        return 0;
    }
}
