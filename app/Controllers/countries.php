<?php

namespace app\Controllers;

use app\Helpers\Bootstrap;
use app\Helpers\Output;
use app\Helpers\Access;

class Countries extends Controller
{

    /**
     * Get all countries from the database
     * @return void
     */
    public function getAll(): void
    {
        $countries = $this->model->getAll();
        Output::render('countries', $countries);
    }

    /**
     * Get all countries and return them as a dropdown menu
     * @return void
     */
    public function countriesList(): void
    {
        $countries = $this->model->getAll();
        Output::render('userDropdownModal', $countries);
    }
}
