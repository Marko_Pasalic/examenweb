<?php

namespace app\Controllers;

use app\Helpers\Output;

class Role extends Controller
{

    const ADMIN = 1;
    const TEACHER = 2;
    const STUDENT = 3;
    const GUEST = 4;
    const BANNED = 5;

    /**
     * Summary of rolesList
     * @return void
     */
    public function rolesList(): void
    {
        $roles = $this->model->getAll();
        Output::render('userDropdownModal', $roles);
    }
}
