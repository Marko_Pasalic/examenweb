<?php

namespace app\Controllers;

use app\Helpers\Helper;
use app\Helpers\Access;
use app\Helpers\Output;
use app\Helpers\Text;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class User extends Controller
{
    protected static array $images = [
        'image/gif',
        'image/png',
        'image/jpeg'
    ];

    /**
     * Summary of getBearer
     * @param int $id
     * @param int $delay
     * @param bool $return
     * @return mixed
     */
    public function getBearer(int $id, int $delay = 3600, bool $return = true): mixed
    {
        $now = time();
        $payload = [
            'iss' => 'http://localhost',    //issuer
            'aud' => 'https://examenweb.net',    //audience
            'jti' => 'web-userid',          //unique id
            'exp' => $now + $delay,         //expiration time
            'sub' => 'user',                //subject
            'iat' => $now,                  //issued at (created)
            'nbf' => $now                   //not before
        ];
        $jwt = JWT::encode($payload, JWT_KEY, 'HS256');
        if ($return) {
            return $jwt;
        } else {
            echo $jwt;
        }
        return 0;
    }

    /**
     * @param int $id
     * @param string $token
     * @return void
     */
    public function updateToken(int $id, string $token): void
    {
        $this->model->updateFieldById('token', $token, $id);
        $_SESSION['token'] = $this->getBearer($id);
    }

    /**
     * Summary of create
     * @return void
     */
    public function create(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        if (
            !empty($_POST['login'])
            && !empty($_POST['password'])
            && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['lastname'])
            && !empty($_POST['firstname'])
            && !empty($_POST['lang'])
        ) {

            if (!in_array($_POST['lang'], ['en', 'fr'])) {
                Output::createAlert(Text::getString(['lang mismatch', 'Problème de langue']), 'danger', 'index.php?view=user/registration');
            }
            $user_data = [
                'login' => trim($_POST['login']),
                'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                'email' => htmlentities($_POST['email'], ENT_QUOTES),
                'lastname' => htmlentities($_POST['lastname'], ENT_QUOTES),
                'firstname' => htmlentities($_POST['firstname'], ENT_QUOTES),
                'nationality' => htmlentities($_POST['nationality'], ENT_QUOTES),
                'lang' => htmlentities($_POST['lang'], ENT_QUOTES),
                'birthdate' => htmlentities($_POST['birthdate'], ENT_QUOTES),
                'address' => htmlentities($_POST['address'], ENT_QUOTES),
                'countries' => htmlentities($_POST['countries'], ENT_QUOTES),
                'postalcode' => htmlentities($_POST['postalcode'], ENT_QUOTES),
                'phonenumber' => htmlentities(is_numeric($_POST['phonenumber']), ENT_QUOTES),
            ];

            if (!$this->model->getByUsernameOrEmail($user_data['login'], $user_data['email'])) {
                $userid = $this->model->create($user_data);
                if ($userid) {

                    $photo_message = false;
                    if (!empty($_FILES['photo']['name'])) {
                        $user = $this->get($userid);
                        if (!$this->manageUploadedFile($user, true)) {
                            $photo_message = Text::getStringFromKey('import');
                        }
                    }
                    $this->addRole($userid, Role::GUEST);
                    Output::render('messageBox', 'Utilisateur ' . $_POST['login'] . ' créer avec succès', 'success');
                    if (!empty($photo_message)) {
                        Output::render('messageBox', $photo_message);
                    }
                } else {
                    Output::render('messageBox', Text::getString(['Failed to create account', 'La création du compte à échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['A user with this username or email address already exists', 'Un utilisateur avec cet identifiant ou cette adresse email existe déjà']));
            }
        } else {
            header('Location: index.php?view=user/registration');
            die;
        }
    }

    public function update(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }
        Access::checkProfile($_SESSION['userid']);

        $update = false;
        $langupdate = false;

        $user = $this->get($_SESSION['userid']);

        if (!empty($_POST['email']) && filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) && $_POST['email'] != $user->email) {
            $email = trim($_POST['email']);
            if (!$this->model->getByUsernameOrEmail('', $email)) {
                $user->email = $email;
                $update = true;
            } else {
                Output::createAlert(Text::getString(['This email address already exist!','Cette adresse email existe déjà!']), 'danger', 'index.php?view=api/user/profile/' . $user->id);
            }
        }
        if (!empty($_POST['password']) && $_POST['password'] != $user->password) {
            $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $update = true;
        }
        if (!empty($_POST['lang']) && in_array($_POST['lang'], ['en', 'fr']) && $_POST['lang'] != $user->lang) {
            $user->lang = htmlentities($_POST['lang'], ENT_QUOTES);
            $update = true;
            $langupdate = true;
        }
        if (!empty($_POST['address']) && $_POST['address'] != $user->address) {
            $user->address = htmlentities($_POST['address'], ENT_QUOTES);
            $update = true;
        }
        if (!empty($_FILES['photo']) && !empty($_FILES['photo']['name'])) {
            try {
                $user->image = $this->manageUploadedFile($user);
            } catch (\Exception $e) {
                Output::createAlert($e->getMessage(), 'danger', 'index.php?view=api/user/profile/' . $user->id);
            }
            $update = true;
        }
        if ($update) {
            $now = new \DateTime();
            $user->updated = $now->setTimezone(new \DateTimeZone('Europe/Paris'))->format('Y-m-d H:i:s');
        }

        if ($update && $this->model->update($user)) {
            $this->addRole($user->id, Role::GUEST);
            if ($langupdate) {
                $_SESSION['lang'] = $_POST['lang'];
            }
            Output::createAlert(Text::getString(['User account update was successful', 'La mise à jour du compte utilisateur a été effectuée avec succès']), 'success', 'index.php?view=api/user/profile/' . $user->id);
        } else {
            Output::createAlert(Text::getString(['User account update failed', 'La mise à jour du compte utilisateur a échoué']), 'danger', 'index.php?view=api/user/profile/' . $user->id);
        }
    }

    public function login(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        if (!empty($_POST['login'] && !empty($_POST['password']))) {
            $user = $this->model->getByField('username', trim($_POST['login']));
            if (!$user) {
                Output::createAlert(Text::getString(['This user doesn\'t exist','Cet utilisateur n\'existe pas']), 'danger', 'index.php?view=/user/login');
            }

            Access::checkBanned($user->id);

            if (password_verify($_POST['password'], $user->password)) {
                if ($this->model->updateFieldById('lastlogin', 'NOW()', $user->id)) {
                    session_destroy();
                    session_name('WEB' . date('Ymd'));
                    session_id(bin2hex(openssl_random_pseudo_bytes(32)));
                    session_start(['cookie_lifetime' => 3600]);

                    $_SESSION['userid'] = $user->id;
                    $_SESSION['lang'] = $user->lang;

                    Output::createAlert(Text::getStringFromKey('welcome') . $user->username, 'success', 'index.php?view=view/default');
                }
            } else {
                Output::render('messagebox', Text::getString(['The password is invalid','Le mot de passe est invalide']));
            }
        }
    }
    /**
     * Summary of logout
     * @return void
     */
    public function logout(): void
    {
        session_unset();
        session_destroy();
        session_write_close();
        header('Location: index.php');
        die;
    }

    /**
     * Summary of getUserIdByName
     * @param mixed $name
     * @return int
     */
    public function getUserIdByName($name): int{
        return $this->model->getUserIdByName($name);
    }

    /**
     * Summary of getAllName
     * @return void
     */
    public function getAllName(): void
    {
        $usersname = $this->model->getAllName();
        Output::render('genericDropdownModal', $usersname);
    }

    /**
     * Summary of getStudentTeacherName
     * @return void
     */
    public function getStudentTeacherName(): void
    {
        $usersname = $this->model->getStudentTeacherName();
        Output::render('genericDropdownModal', $usersname);
    }

    /**
     * Summary of getTeacher
     * @return void
     */
    public function getTeacher(): void
    {
        $usersname = $this->model->getTeacher();
        Output::render('genericDropdownModal', $usersname);
    }
    
    /**
     * Summary of getForAdmin
     * @return array
     */
    public function getForAdmin()
    {
        return $this->model->getForAdmin();
    }

    /**
     * Summary of admin
     * @return void
     */
    public function admin(): void
    {
        Access::checkAdmin();

        $user = new User();
        $users = $user->model->getAll();

        Output::render('admin', $users);
    }

    /**
     * Summary of usersList
     * @return void
     */
    public function usersList(): void
    {
        Access::checkAdmin();

        $user = new User();
        $users = $user->getForAdmin();

        Output::render('users', $users);
    }

    /**
     * Summary of getUserModalBody
     * @param int $id
     * @param int $callerid
     * @param string $token
     * @return void
     */
    public function getUserModalBody(int $id, int $callerid, string $token): void
    {
        if (!Access::checkToken($token, 'modal-user-token', $callerid)) {
            header('HTTP/1.1 403');
            die;
        }

        $data = new \stdClass();
        $data->user = $this->get($id);
        $data->role = $this->getRole($id);
        $data->courseslist = $this->profileCourses($id);

        Output::render('usermodal', $data);
    }

    /**
     * Summary of exportProfile
     * @param int $id
     * @return void
     */
    public function exportProfile(int $id): void
    {
        $user = $this->getForProfile($id);
        $userForProfile = self::formatForProfile($user);
        $userForProfile->format = 'json';

        Output::render('exportProfile', $userForProfile);
    }

    /**
     * Summary of exportImage
     * @param int $id
     * @return void
     */
    public function exportImage(int $id): void
    {
        $user = $this->getForProfile($id);
        $userForProfile = self::formatForProfile($user);
        Output::render('exportImage', $userForProfile);
    }

    /**
     * Summary of exportCourseList
     * @param int $id
     * @return void
     */
    public function exportCourseList(int $id): void
    {
        $course = new Course();
        $courses = $course->getByUserEnrol($id);

        Output::render('exportCourseList', $courses);
    }

    /**
     * Summary of isAdmin
     * @param int $id
     * @return bool
     */
    public function isAdmin(int $id): bool
    {
        return $this->model->isAdmin($id);
    }

    /**
     * Summary of isBanned
     * @param int $id
     * @return bool
     */
    public function isBanned(int $id): bool
    {
        return $this->model->isBanned($id);
    }

    /**
     * Summary of getRole
     * @param int $id
     * @return mixed
     */
    public function getRole(int $id)
    {
        return $this->model->getRole($id);
    }

    /**
     * Summary of hasRole
     * @param int $id
     * @param int $roleid
     * @return bool
     */
    public function hasRole(int $id, int $roleid): bool
    {
        return $this->model->hasRole($id, $roleid);
    }

    /**
     * Summary of hasAnyRole
     * @param int $id
     * @return bool
     */
    public function hasAnyRole(int $id): bool
    {
        return $this->model->hasAnyRole($id);
    }

    /**
     * Summary of addRole
     * @param int $id
     * @param int $roleid
     * @return void
     */
    public function addRole(int $id, int $roleid): void
    {
        $this->model->updateRole($id, $roleid);
    }

    /**
     * Summary of updateRole
     * @param int $id
     * @param int $roleid
     * @return void
     */
    public function updateRole(int $id, int $roleid): void
    {
        $this->model->updateRole($id, $roleid);
    }

    /**
     * Summary of updateByRoleName
     * @param int $id
     * @param string $rolename
     * @return void
     */
    public function updateByRoleName(int $id, string $rolename): void
    {
        $this->model->updateByRoleName($id, $rolename);
        $this->model->setAdmin($id, 0);
        if ($rolename == "admin") {
            $this->model->setAdmin($id, 1);
        }
    }

    /**
     * Summary of profile
     * @param int $id
     * @return void
     */
    public function profile(int $id): void
    {
        Access::checkProfile($id);

        $user = $this->getForProfile($id);
        $userForProfile = self::formatForProfile($user);

        Output::render('profile', $userForProfile);
        //Output::render('profileUpdate', $user);

        $course = new Course();
        $courses = $course->getByUserEnrol($id);

        Output::render('profileCourses', $courses);
    }

    /**
     * Summary of profileCourses
     * @param int $id
     * @param bool $return
     * @return mixed
     */
    public function profileCourses(int $id, bool $return = true): mixed
    {
        $course = new Course();
        $courses = $course->getByUserEnrol($id);
        if ($return) {
            return Output::get('profileCourses', $courses);
        } else {
            echo Output::get('profileCourses', $courses);
        }
        return 0;
    }

    /**
     * Summary of getForProfile
     * @param int $id
     * @return mixed
     */
    protected function getForProfile(int $id): mixed
    {
        $user = $this->get($id);
        return $user;
    }

    /**
     * Summary of formatForProfile
     * @param object $user
     * @return object
     */
    protected function formatForProfile(object $user): object
    {
        $userForProfile = clone $user;

        unset($userForProfile->id);
        unset($userForProfile->password);
        if (!$userForProfile->admin) {
            $userForProfile->admin = 'Non';
        } else {
            $userForProfile->admin = 'Oui';
        }
        if ($userForProfile->lang == 'fr') {
            $userForProfile->birthdate = date_format(new \DateTime($userForProfile->birthdate), "d/m/Y");
            $userForProfile->created = date_format(new \DateTime($userForProfile->created), "d/m/Y H\hi");
            $userForProfile->lastlogin = date_format(new \DateTime($userForProfile->lastlogin), "d/m/Y H\hi");
        }

        return $userForProfile;
    }

    /**
     * Summary of manageUploadedFile
     * @param int|object $user
     * @param bool $update
     * @throws \Exception
     * @return mixed
     */
    protected function manageUploadedFile(int|object $user, bool $update = false): mixed
    {
        if (!is_object($user)) {
            $user = $this->model->get($user);
        }
        if (
            !empty($_FILES['photo']['tmp_name']) &&
            !empty($_FILES['photo']['name']) &&
            is_uploaded_file($_FILES['photo']['tmp_name']) &&
            in_array($_FILES['photo']['type'], self::$images)
        ) {
            $photopath = 'image' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
            $imagepath = getcwd() . DIRECTORY_SEPARATOR . $photopath;
            if (!is_dir($imagepath)) {
                mkdir($imagepath, 0755, true);
            }
            $ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
            $dest = $imagepath . $user->id . '.' . $ext;
            $url = $photopath . $user->id . '.' . $ext;
            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $dest);
            if ($move) {
                if (!empty($user->image) && $url != $user->image && pathinfo($user->image, PATHINFO_EXTENSION) != $ext) {
                    $existing_file_image = ROOT_PATH . DIRECTORY_SEPARATOR . $user->image;
                    if (file_exists($existing_file_image)) {
                        unlink($existing_file_image);
                    }
                }
                $user->image = $url;
                if ($update) {
                    return $this->model->update($user);
                }
            } else {
                throw new \Exception(Text::getStringFromKey('upload'));
            }
        } else {
            $output_txt = [
                'File error.',
                'Le fichier a été refusé.',
            ];
            if (!empty($_FILES['photo']['type']) && !in_array($_FILES['photo']['type'], self::$images)) {
                $output_txt[0] .= ' File extension must be ' . implode(' ou ', self::$images) . '.';
                $output_txt[1] .= ' Le format de fichier doit être ' . implode(' ou ', self::$images) . '.';
            }
            if ($_FILES['photo']['error']) {
                switch ($_FILES['photo']['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $output_txt[0] .= ' No file sent.';
                        $output_txt[1] .= ' Aucun fichier envoyé.';
                        break;
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        $output_txt[0] .= ' File size must be lower than ' . Helper::getMaxFileSizeHumanReadable() . '.';
                        $output_txt[1] .= ' La taille du fichier doit être inférieure à ' . Helper::getMaxFileSizeHumanReadable() . '.';
                        break;
                    default:
                        $output_txt[0] .= ' Unknown error.';
                        $output_txt[1] .= ' Erreur inconnue.';
                }
            }
            throw new \Exception(Text::getString($output_txt));
        }
        return $user->image;
    }
}
