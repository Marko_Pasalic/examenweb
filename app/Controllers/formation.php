<?php

namespace app\Controllers;

use app\Helpers\Access;
use app\Helpers\Output;
use app\Helpers\Text;

class Formation extends Controller
{

    /**
     * Summary of createFormation
     * @return void
     */
    public function createFormation(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();

        if (
            !empty($_POST['name'])
            && !empty($_POST['degree'])
            && !empty($_POST['status'])
            && !empty($_POST['begindate'])
            && !empty($_POST['enddate'])
        ) {


            $formation_data = [
                'name' => (htmlentities($_POST['name'], ENT_QUOTES)),
                'degree' => (htmlentities($_POST['degree'], ENT_QUOTES)),
                'status' => htmlentities($_POST['status'], ENT_QUOTES),
                'begindate' => htmlentities($_POST['begindate'], ENT_QUOTES),
                'enddate' => htmlentities($_POST['enddate'], ENT_QUOTES),
            ];

            if (!$this->model->getByFormationName($formation_data['name'])) {
                $formationid = $this->model->createFormation($formation_data);
                if ($formationid) {
                    Output::createAlert(Text::getString(['Formation ' . $_POST['name'] . 'has been successfully added', 'La formation ' . $_POST['name'] . ' a été ajoutée avec succès']), 'success', 'index.php?view=api/formation/list');
                } else {
                    Output::render('messageBox', Text::getString(['Failed to create course', 'La création de la formation à échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['A formation with this name already exists', 'Une formation avec ce nom existe déjà']));
            }
        } else {
            header('Location: index.php?view=view/admin/createformation');
            die;
        }
    }

    /**
     * Summary of assignCourse
     * @return void
     */
    public function assignCourse(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();


        if (
            !empty($_POST['formation-name'])
            && !empty($_POST['course-name'])
            && !empty($_POST['period'])
            && !empty($_POST['prerequisite'])
        ) {
            $course = new course();

            if ($_POST['prerequisite'] == $_POST['course-name']) {
                Output::createAlert(Text::getString(['Please choose a valid prerequisite', 'Veuillez choisir un prérequis valable']), 'danger', 'index.php?view=view/admin/assignCourse');
            }

            if (isset($_POST['determinant'])) {
                $_POST['determinant'] = 1;
            } else {
                $_POST['determinant'] = 0;
            }

            $formation_data = [
                'formationid' => $this->model->getFormationIdByName($_POST['formation-name']),
                'courseid' => $course->getcourseIdByName($_POST['course-name']),
                'period' => $_POST['period'],
                'determinant' => $_POST['determinant'],
                'prerequisite' => $course->getcourseIdByName($_POST['prerequisite']),
                'teacher' => $_POST['teacher'],
            ];
            if (!$this->model->hasAssignCourse($formation_data['formationid'], $formation_data['courseid'])) {
                $formationAssign = $this->model->AssignCourse($formation_data);
                if ($formationAssign) {
                    Output::createAlert(Text::getString(['Course has been successfully assigned', 'Le cours a été assigné avec succès']), 'success', 'index.php?view=api/formation/list');
                } else {
                    Output::render('messageBox', Text::getString(['Failed to create course', 'La création de la formation à échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['This course is already assigned to this formation', 'Ce cours est déjà assigné à cette formation']));
            }
        } else {
            header('Location: index.php?view=api/formation/list');
            die;
        }
    }

    /**
     * Summary of unassignCourse
     * @return void
     */
    public function unassignCourse(): void
    {
        Access::checkAdmin();

        if (
            !empty($_POST['formation-name'])
            && !empty($_POST['course-name'])
        ) {

            $formation_data = [
                'formationid' => $_POST['formation-name'],
                'courseid' => $_POST['course-name'],
            ];
            if ($this->model->hasAssignCourse($formation_data['formationid'], $formation_data['courseid'])) {
                $formationUnassign = $this->model->unassignCourse($formation_data);
                if ($formationUnassign) {
                    Output::createAlert(Text::getString(['The course has been successfully unassigned from formation', 'Le cours a été désassigné de la formation avec succès']), 'success', 'index.php?view=api/formation/list');
                } else {
                    Output::render('messageBox', Text::getString(['The course could not be removed from formation', 'Le cours n\'a pas pu être retirer de la formation']));
                }
            }
            Output::render('messageBox', Text::getString(['The course is not assigned to this formation', 'Le cours n\est pas assigné à cette formation']));
        }
    }

    /**
     * Summary of update
     * @return void
     */
    public function update(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();
        $update = false;
        $langupdate = false;

        $formation = $this->get($_POST['id']);

        if (!empty($_POST['degree'])) {
            $formation->degree = $_POST['degree'];
            $update = true;
        }

        if (!empty($_POST['status'])) {
            $formation->status = $_POST['status'];
            $update = true;
        }

        $formation->begindate = $_POST['begindate'];
        $formation->enddate = $_POST['enddate'];

        $update = true;

        if ($update && $this->model->update($formation)) {
            Output::createAlert(Text::getString(['Formation update completed successfully', 'La mise à jour de la formation a été effectuée avec succès']), 'success', 'index.php?view=api/formation/list');
        } else {
            Output::createAlert(Text::getString(['Formation update failed', 'La mise à jour de la formation a échoué']), 'danger', 'index.php?view=api/formation/list');
        }
    }

    /**
     * Summary of updateForm
     * @param int $id
     * @return void
     */
    public function updateForm(int $id): void
    {
        Access::checkAdmin();

        $formation = $this->get($id);

        Output::render('formationUpdate', $formation);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        Access::checkAdmin();


        if ($this->model->hasCourse($id)) {
            $this->model->deleteCourses($id);
        }
        if (!$this->model->hasGroup($id)) {
            if ($this->model->delete($id)) {
                Output::createAlert(Text::getString(['Training deletion was successful', 'La suppression de la formation a été effectuée avec succès']), 'success', 'index.php?view=api/formation/list');
            } else {
                Output::createAlert(Text::getString(['Delete formation failed', 'La suppression de la formation a échouée']), 'danger', 'index.php?view=api/formation/list');
            }
        } else {
            Output::createAlert(Text::getString(['You cannot delete a training with a linked group', 'Vous ne pouvez supprimer une formation comportant un groupe lié']), 'danger', 'index.php?view=api/formation/list');
        }
    }

    /**
     * Summary of list
     * @return void
     */
    public function list(): void
    {
        Access::checkAdmin();
        $formation = $this->model->getAll();

        Output::render('formations', $formation);
    }

    /**
     * Summary of getAllName
     * @return void
     */
    public function getAllName(): void
    {
        $formationName = $this->model->getAllName();
        Output::render('genericDropdownModal', $formationName);
    }

    /**
     * Summary of getFormationIdByName
     * @param string $formationName
     * @return int
     */
    public function getFormationIdByName(string $formationName)
    {
        $id = $this->model->getFormationIdByName($formationName);
        return $id;
    }
}
