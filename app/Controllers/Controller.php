<?php

namespace app\Controllers;

use stdClass;
use app\Models\Model;

abstract class Controller
{
    protected int $id;
    protected mixed $data;
    protected Model $model;
    protected static mixed $static_model = null;

    /**
     * When instantiating the class, binds the associated Model as a static property of the Controller
     *
     * @param int|null $id
     */
    public function __construct(int $id = null)
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        $class = '\app\Models\\' . end($data);
        $this->model = new $class();
        self::$static_model = $this->model;
        if (!empty($id)) {
            $this->id = $id;
            $this->data = $this->model->get($id);
        }
    }

    /**
     * Retrieves, in the form of an object, the content of the record in DB based on its id
     *
     * @param int $id
     * @return mixed
     */
    public function get(int $id): mixed
    {
        return $this->model->getByField('id', $id);
    }

    /**
     * Injects properties of an object as properties of the instantiated Controller object
     *
     * @param stdClass $object
     * @return void
     */
    public function import(stdClass $object): void
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }
}
