<?php

namespace app\Controllers;

use app\Helpers\Bootstrap;
use app\Helpers\Output;
use app\Helpers\Access;
use app\Helpers\Text;

class Course extends Controller
{
    /**
     * list of courses
     * @return void
     */
    public function list(): void
    {
        Access::checkLoggedIn();

        $courses = $this->model->getAllForUsers();

        Output::render('courses', $courses);
    }

    /**
     * List of courses for the admin view
     * @return void
     */
    public function listAdmin(): void
    {
        Access::isAdmin();

        $courses = $this->model->getAll();

        Output::render('coursesListAdmin', $courses);
    }

    /**
     * Allow user to enrol for a course
     * @param int $courseid
     * @param int $userid
     * @return int
     */
    public function getEnrol(int $courseid, int $userid): int
    {
        return $this->model->getEnrol($courseid, $userid);
    }

    /**
     * Summary of getByUserEnrol
     * @param int $userid
     * @return array
     */
    public function getByUserEnrol(int $userid): array
    {
        return $this->model->getByUserEnrol($userid);
    }

    /**
     * Summary of getAllName
     * @return void
     */
    public function getAllName(): void
    {
        $coursesname = $this->model->getAllName();
        Output::render('genericDropdownModal', $coursesname);
    }

    /**
     * Summary of getcourseIdByName
     * @param mixed $coursename
     * @return int
     */
    public function getcourseIdByName($coursename): int
    {
        $id = $this->model->getcourseIdByName($coursename);
        return $id;
    }

    /**
     * Summary of enrol
     * @param int $courseid
     * @param int $userid
     * @return void
     */
    public function enrol(int $courseid, int $userid): void
    {
        if (self::getEnrol($courseid, $userid)) {
            
            Output::createAlert(Text::getString(['User already enrolled in this course', 'Utilisateur déjà inscrit à ce cours']), 'danger', 'index.php?view=api/course/list');
        } else {

            $prereq = $this->model->getPreprequisite($courseid);
            if ($prereq && !self::getEnrol($prereq, $userid)) {
                Output::createAlert(Text::getString(['Registration failed, prerequisites not met', 'L\'inscription a échoué, les prérequis ne sont pas satisfaits']), 'danger', 'index.php?view=api/course/list');
            }

            if ($this->model->enrol($courseid, $userid)) {
                Output::createAlert(Text::getString(['User registered successfully', 'Utilisateur inscrit avec succès']), 'success', 'index.php?view=api/course/list');
            } else {
                Output::createAlert(Text::getString(['Registration failed', 'L\'inscription a échoué']), 'danger', 'index.php?view=api/course/list');
            }
        }
    }

    /**
     * Summary of getByFormationForForm
     * @param int $formationid
     * @return void
     */
    public function getByFormationForForm(int $formationid): void
    {
        $data = $this->model->getByFormationForForm($formationid);
        Output::render('getFormOptions', $data);
    }

    /**
     * Summary of getForForm
     * @param int $id
     * @return void
     */
    public function getForForm(int $id): void
    {
        $data = $this->model->getByFormation($id);
        Output::render('getFormOptions', $data);
    }

    /**
     * Summary of createCourse
     * @return void
     */
    public function createCourse(): void
    {

        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();

        if (
            !empty($_POST['name'])
            && !empty($_POST['code'])
            && !empty($_POST['status'])
        ) {

            $course_data = [
                'name' => (htmlentities($_POST['name'], ENT_QUOTES)),
                'code' => htmlentities($_POST['code'], ENT_QUOTES),
                'status' => htmlentities($_POST['status'], ENT_QUOTES),
            ];




            if (!$this->model->getByCourseName($course_data['name'])) {
                if (!$this->model->getByCode($course_data['code'])) {
                    $courseid = $this->model->createCourse($course_data);
                }
                if ($courseid) {
                    Output::createAlert(Text::getString(['The course ' . $_POST['name'] . 'has been added successfully', 'Le cours ' . $_POST['name'] . ' a été ajouté avec succès']), 'success', 'index.php?view=api/course/listAdmin');
                } else {
                    Output::render('messageBox', Text::getString(['Failed to create course', 'La création du cours à échoué']));
                }
            } else {
                Output::render('messageBox', Text::getString(['A course with this label already exists', 'Un cours avec ce libellé existe déjà']));
            }
        } else {
            header('Location: index.php?view=view/admin/createCourse');
            die;
        }
    }

    /**
     * Summary of update
     * @return void
     */
    public function update(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkAdmin();

        $update = false;

        $course = $this->get($_POST['id']);

        if (
            !empty($_POST['name'])
            && !empty($_POST['status'])
        ) {

            if (!$this->model->getByCourseName($_POST['name'])) {
                $course->name = $_POST['name'];
                $update = true;
            } else {
                Output::render('messageBox', 'Ce cours existe déjà!');
            }

            if (!empty($_POST['status']) && $_POST['status'] != $course->status) {
                $course->status = $_POST['status'];
                $update = true;
            }
        }

        if ($update && $this->model->update($course)) {
            Output::createAlert(Text::getString(['Course update completed successfully', 'La mise à jour du cours a été effectuée avec succès']), 'success', 'index.php?view=api/course/listAdmin');
        } else {
            Output::createAlert(Text::getString(['The update has failed', 'La mise à jour de la formation a échoué']), 'danger', 'index.php?view=api/course/listAdmin');
        }
    }

    /**
     * Summary of updateForm
     * @param int $id
     * @return void
     */
    public function updateForm(int $id): void
    {
        Access::checkAdmin();

        $course = $this->get($id);

        Output::render('courseUpdate', $course);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        Access::checkAdmin();

        if ($this->model->hasFormation($id)) {
            Output::createAlert(Text::getString(['This course is linked to a training and therefore cannot be deleted' ,'Ce cours est lié à une formation et ne peut donc pas être supprimé']), 'danger', 'index.php?view=api/course/listAdmin');
        }
        if ($this->model->delete($id)) {
            Output::createAlert(Text::getString(['Course deleted successfully', 'La suppression du cours a été effectuée avec succès']), 'success', 'index.php?view=api/course/listAdmin');
        } else {
            Output::createAlert(Text::getString(['Course deletion failed','La suppression du cours a échouée']), 'danger', 'index.php?view=api/course/listAdmin');
        }
    }

    /**
     * Summary of unassignCourse
     * @return void
     */
    public function unassignCourse(): void
    {
        Access::checkAdmin();

        $formation = new Formation();
        $data = $formation->model->getAllForForm('name');
        Output::render('unassignCourse', $data);
    }
}
