<?php

namespace app\Models;

use app\Controllers\Role;
use PDO;

class User extends Model
{
    protected int $id;
    protected string $username;
    protected string $password;
    protected string $email;
    protected string $lastname;
    protected string $firstname;
    protected string $nationality;
    protected string $birthdate;
    protected string $address;
    protected int $postalcode;
    protected int $phonenumber;
    protected string $photo;
    protected string $created;
    protected string $lastlogin;
    protected bool $admin;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Summary of create
     * @param array $data
     * @return int
     */
    public static function create(array $data): int
    {
        $insert = self::$connect->prepare("INSERT INTO user (username, `password`, email, lastname, firstname, nationality, lang, birthdate, `address`, countries, postalcode, phonenumber, created, lastlogin)
            VALUES (?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())");
        $insert->execute(array_values($data));
        if ($insert->rowCount()) {
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of getAll
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array
    {
        $users = [];
        $sql = 'SELECT * 
                FROM user u
                JOIN user_role ur ON ur.userid = u.id
                JOIN role r on r.id = ur.roleid
                ORDER BY u.id';

        $result = self::$connect->prepare($sql);
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $users[] = $data_tmp;
        }
        return $users;
    }

    /**
     * Summary of getAllName
     * @param string $orderby
     * @return array
     */
    public static function getAllName(string $orderby = ''): array
    {
        $usersname = [];
        $sql = 'SELECT `username` 
                FROM user';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $usersname[] = $data_tmp;
        }
        return $usersname;
    }

    /**
     * Summary of getStudentTeacherName
     * @param string $orderby
     * @return array
     */
    public static function getStudentTeacherName(string $orderby = ''): array
    {
        $usersname = [];
        $sql = 'SELECT username 
                FROM user u
                JOIN user_role ur ON u.id = ur.userid
                JOIN role r ON ur.roleid = r.id
                WHERE r.name = "student" OR r.name = "teacher"';
        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $usersname[] = $data_tmp;
        }
        return $usersname;
    }

    /**
     * Summary of getTeacher
     * @param string $orderby
     * @return array
     */
    public static function getTeacher(string $orderby = ''): array
    {
        $usersname = [];
        $sql = 'SELECT username 
                FROM user u
                JOIN user_role ur ON u.id = ur.userid
                JOIN role r ON ur.roleid = r.id
                WHERE r.name = "teacher"';
        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $usersname[] = $data_tmp;
        }
        return $usersname;
    }

    /**
     * Summary of getForAdmin
     * @param string $orderby
     * @return array
     */
    public static function getForAdmin(string $orderby = ''): array
    {
        $users = [];
        $sql = 'SELECT u.id, u.username, u.email, u.created, u.lastlogin, u.lang, r.name as rolename
                FROM user u
                JOIN user_role ur ON ur.userid = u.id
                JOIN role r on r.id = ur.roleid
                ORDER BY u.id';

        $result = self::$connect->prepare($sql);
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $users[] = $data_tmp;
        }
        return $users;
    }

    /**
     * Summary of getByUsernameOrEmail
     * @param string $login
     * @param string $email
     * @return int
     */
    public static function getByUsernameOrEmail(string $login, string $email): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE username = ? OR email = ?");
        $result->execute([$login, $email]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getUserIdByName
     * @param string $name
     * @return int
     */
    public static function getUserIdByName(string $name): int{
        $result = self::$connect->prepare("SELECT id FROM user WHERE `username` = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of isAdmin
     * @param int $id
     * @return mixed
     */
    public static function isAdmin(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE roleid = " . Role::ADMIN . " AND userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of setAdmin
     * @param int $id
     * @param int $admin
     * @return bool
     */
    public static function setAdmin(int $id, int $admin)
    {
        $request = self::$connect->prepare(
            "UPDATE user u 
            SET u.admin = ?
            WHERE u.id = ?"
        );
        $request->execute([$admin, $id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of isBanned
     * @param int $id
     * @return mixed
     */
    public static function isBanned(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE roleid = " . Role::BANNED . " AND userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getRole
     * @param int $id
     * @return mixed
     */
    public static function getRole(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT r.name as rolename 
                                            FROM user_role ur
                                            JOIN role r ON ur.roleid = r.id
                                            WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasRole
     * @param int $id
     * @param int $role
     * @return mixed
     */
    public static function hasRole(int $id, int $role): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE userid = ? AND roleid = ?");
        $result->execute([$id, $role]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasRoleByName
     * @param int $id
     * @param string $role
     * @return mixed
     */
    public static function hasRoleByName(int $id, string $role): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role ur, role r WHERE ur.roleid = r.id AND ur.userid = ? AND r.name = ?");
        $result->execute([$id, $role]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasAnyRole
     * @param int $id
     * @return mixed
     */
    public static function hasAnyRole(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of addRole
     * @param int $id
     * @param int $roleid
     * @return bool
     */
    public static function addRole(int $id, int $roleid): bool
    {
        $request = self::$connect->prepare("INSERT INTO user_role (userid, roleid, created) VALUES (?, ?, NOW())");
        $request->execute([$id, $roleid]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of updateRole
     * @param int $id
     * @param int $roleid
     * @return bool
     */
    public static function updateRole(int $id, int $roleid): bool
    {
        $request = self::$connect->prepare("INSERT INTO user_role (userid, roleid, created) VALUES (?, ?, NOW()) ON DUPLICATE KEY UPDATE roleid = ?");
        $request->execute([$id, $roleid, $roleid]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of updateByRoleName
     * @param int $id
     * @param string $rolename
     * @return bool
     */
    public static function updateByRoleName(int $id, string $rolename): bool
    {
        $request = self::$connect->prepare(
            "UPDATE user_role AS ur 
        SET ur.roleid = (SELECT id FROM role WHERE name = ?), 
        created = NOW()
        WHERE userid = ?"
        );
        $request->execute([$rolename, $id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }
}
