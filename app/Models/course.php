<?php

namespace app\Models;

class Course extends Model
{

    /**
     * createCourse
     * @param array $data
     * @return int
     */
    public static function createCourse(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO course (name, code ,status)
        VALUES (?, ?, ?)');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of updateCourse
     * @param int $id
     * @return bool
     */
    public static function updateCourse(int $id): bool{
        $request = self::$connect->prepare("INSERT INTO course (`name`, code, `status`) VALUES (?, ?, ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of delete
     * @param int $id
     * @return bool
     */
    public static function delete(int $id): bool
    {
        $request = self::$connect->prepare("DELETE  FROM course WHERE id = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Get a list of all courses
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array
    {
        $courses = [];
        $sql = 'SELECT  id as courseid,
		                name as coursename,
                        code as code,
                        status as status
                FROM course';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $courses[] = $data_tmp;
        }
        return $courses;
    }

    /**
     * Get a list of all courses link to a formation
     * @param string $orderby
     * @return array
     */
    public static function getAllCourse_formation(string $orderby = ''): array
    {
        $courses = [];
        $sql = 'SELECT c.id as courseid, 
                f.name as formation_name,
                f.degree as formation_degree,
                c.name as course_name, 
                fc.period as periods, 
                fc.determinant as det, 
                c2.name as course_prereq, 
                fc.teacher as teacher
               FROM formation_course fc
               JOIN formation f ON f.id = fc.formationid
               JOIN course c ON c.id = fc.courseid
               LEFT JOIN formation_course fc2 ON fc2.id = fc.prepreq
               LEFT JOIN course c2 ON c2.id = fc2.courseid
               ORDER BY fc.formationid';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $courses[] = $data_tmp;
        }
        return $courses;
    }

    /**
     * Show only courses that is active
     * @param string $orderby
     * @return array
     */
    public static function getAllForUsers(string $orderby = ''): array
    {
        $courses = [];
        $sql = 'SELECT c.id as courseid, 
                f.name as formation_name,
                f.degree as formation_degree,
                c.name as course_name, 
                fc.period as periods, 
                fc.determinant as det, 
                c2.name as course_prereq, 
                fc.teacher as teacher
               FROM formation_course fc
               JOIN formation f ON f.id = fc.formationid
               JOIN course c ON c.id = fc.courseid
               LEFT JOIN formation_course fc2 ON fc2.id = fc.prepreq
               LEFT JOIN course c2 ON c2.id = fc2.courseid
               WHERE f.status = "active"
               ORDER BY fc.formationid';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $courses[] = $data_tmp;
        }
        return $courses;
    }

    /**
     * Summary of getAllName
     * @param string $orderby
     * @return array
     */
    public static function getAllName(string $orderby = ''): array
    {
        $coursesname = [];
        $sql = 'SELECT `name` 
                FROM course';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $coursesname[] = $data_tmp;
        }
        return $coursesname;
    }

    /**
     * Summary of getByFormation
     * @param int $formationid
     * @return array
     */
    public static function getByFormation(int $formationid): array
    {
        $courses = [];
        $sql = 'SELECT c.id as courseid, 
                f.name as formation_name,
                f.degree as formation_degree,
                c.name as course_name, 
                fc.period as periods, 
                fc.determinant as det, 
                c2.name as course_prereq, 
                fc.teacher as teacher
               FROM formation_course fc
               JOIN formation f ON f.id = fc.formationid
               JOIN course c ON c.id = fc.courseid
               LEFT JOIN formation_course fc2 ON fc2.id = fc.prepreq
               LEFT JOIN course c2 ON c2.id = fc2.courseid
               WHERE fc.formationid = ?
               ORDER BY fc.formationid';

        $request = self::$connect->prepare($sql);
        $request->execute([$formationid]);
        while ($data_tmp = $request->fetchObject()) {
            $courses[] = $data_tmp;
        }
        return $courses;
    }

    /**
     * Summary of getByFormationForForm
     * @param int $formationid
     * @return array
     */
    public static function getByFormationForForm(int $formationid): array
    {
        $courses = [];
        $sql = 'SELECT c.id as courseid, 
                c.name as coursename
               FROM formation_course fc
               JOIN formation f ON f.id = fc.formationid
               JOIN course c ON c.id = fc.courseid
               WHERE fc.formationid = ?
               ORDER BY c.name';

        $request = self::$connect->prepare($sql);
        $request->execute([$formationid]);
        while ($data_tmp = $request->fetchObject()) {
            $courses[$data_tmp->courseid] = $data_tmp->coursename;
        }
        return $courses;
    }

    /**
     * Summary of enrol
     * @param int $courseid
     * @param int $userid
     * @return bool
     */
    public static function enrol(int $courseid, int $userid): bool
    {
        $request = self::$connect->prepare("INSERT INTO user_course (userid, courseid, created) VALUES (?, ?, NOW())");
        $request->execute([$userid, $courseid]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of getEnrol
     * @param int $courseid
     * @param int $userid
     * @return int
     */
    public function getEnrol(int $courseid, int $userid): int
    {
        $request = self::$connect->prepare("SELECT COUNT(*) FROM user_course WHERE userid = ? AND courseid = ?");
        $request->execute([$userid, $courseid]);
        return $request->fetchColumn();
    }

    /**
     * Summary of getByCode
     * @param int $code
     * @return int
     */
    public static function getByCode(int $code): int{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM course WHERE code = ?");
        $result->execute([$code]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getByUserEnrol
     * @param int $userid
     * @return array
     */
    public function getByUserEnrol(int $userid): array
    {
        $courses = [];
        $sql = 'SELECT c.id as courseid,
                    f.name as formation_name,
                    f.degree as formation_degree,
                    c.name as course_name,
                    fc.period as periods,
                    fc.determinant as det,
                    c2.name as course_prereq,
                    fc.teacher as teacher,
                    r.name as role
                FROM formation_course fc
                JOIN formation f ON f.id = fc.formationid
                JOIN course c ON c.id = fc.courseid
                JOIN user_course uc ON uc.courseid = c.id AND uc.userid = ?
                LEFT JOIN formation_course fc2 ON fc2.id = fc.prepreq
                LEFT JOIN course c2 ON c2.id = fc2.courseid
                JOIN user_role ur ON uc.userid = ur.userid
                JOIN role r ON ur.roleid = r.id
                ORDER BY f.name, c.name';

        $request = self::$connect->prepare($sql);
        $request->execute([$userid]);
        while ($data_tmp = $request->fetchObject()) {
            $courses[] = $data_tmp;
        }
        return $courses;
    }

    /**
     * Summary of getByCourseName
     * @param string $name
     * @return int
     */
    public static function getByCourseName(string $name): int{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM course WHERE name = ?");
        $result->execute([$name, ]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getcourseIdByName
     * @param string $name
     * @return int
     */
    public static function getcourseIdByName(string $name): int{
        $result = self::$connect->prepare("SELECT id FROM course WHERE name = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getPreprequisite
     * @param int $courseid
     * @return mixed
     */
    public function getPreprequisite(int $courseid): mixed
    {
        $sql = "SELECT prepreq FROM formation_course WHERE courseid = ?";
        $request = self::$connect->prepare($sql);
        $request->execute([$courseid]);
        return $request->fetchObject()->prepreq;
    }

    /**
     * Summary of hasFormation
     * @param int $id
     * @return mixed
     */
    public static function hasFormation(int $id): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM formation_course WHERE courseid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }
}
