<?php

namespace app\Models;

class Countries extends Model{

    /**
     * Summary of getAll
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array{
        $countries = [];
        $sql = 'SELECT name FROM countries';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while($data_tmp = $request->fetchObject()) {
            $countries[] = $data_tmp;
        }
        return $countries;
    }
}