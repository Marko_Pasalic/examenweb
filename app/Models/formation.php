<?php

namespace app\Models;

class Formation extends Model
{
    /**
     * Summary of createFormation
     * @param array $data
     * @return int
     */
    public static function createFormation(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO formation (name, degree, status, begindate, enddate)
        VALUES (?, ?, ?, ?, ?)');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of AssignCourse
     * @param array $data
     * @return int
     */
    public static function AssignCourse(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO formation_course (formationid, courseid, `period`, determinant, prepreq, teacher)
        VALUES (?, ?, ?, ?, ?, ?)');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of unassignCourse
     * @param array $data
     * @return bool
     */
    public static function unassignCourse(array $data): bool
    {
        $request = self::$connect->prepare("DELETE FROM formation_course  WHERE formationid = ? AND courseid = ?");
        $request->execute(array_values($data));
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of updateFormation
     * @param int $id
     * @return bool
     */
    public static function updateFormation(int $id): bool{
        $request = self::$connect->prepare(
            "INSERT INTO formation (name, degree, status, begindate, enddate) VALUES (?, ?, ?, ?, ?) WHERE id = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of delete
     * @param int $id
     * @return bool
     */
    public static function delete(int $id): bool
    {
        $request = self::$connect->prepare("DELETE FROM formation  WHERE id = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of deleteCourses
     * @param int $id
     * @return bool
     */
    public static function deleteCourses(int $id): bool
    {
        $request = self::$connect->prepare("DELETE FROM formation_course  WHERE formationid = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of hasCourse
     * @param int $id
     * @return mixed
     */
    public static function hasCourse(int $id): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM formation_course WHERE formationid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasAssignCourse
     * @param int $formationid
     * @param int $courseid
     * @return mixed
     */
    public static function hasAssignCourse(int $formationid, int $courseid): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM formation_course WHERE formationid = ? AND courseid = ?");
        $result->execute([$formationid, $courseid]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasGroup
     * @param int $formationid
     * @return mixed
     */
    public static function hasGroup(int $formationid): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_group_formation WHERE formationid = ?");
        $result->execute([$formationid]);
        return $result->fetchColumn();
    }

    /**
     * Summary of hasAssignGroup
     * @param int $formationid
     * @param int $user_group_id
     * @return mixed
     */
    public static function hasAssignGroup(int $formationid, int $user_group_id): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_group_formation WHERE formationid = ? AND user_group_id = ?");
        $result->execute([$formationid, $user_group_id]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getByFormationName
     * @param string $name
     * @return int
     */
    public static function getByFormationName(string $name): int{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM formation WHERE name = ?");
        $result->execute([$name, ]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getFormationIdByName
     * @param string $name
     * @return int
     */
    public static function getFormationIdByName(string $name): int{
        $result = self::$connect->prepare("SELECT id FROM formation WHERE `name` = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getAll
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array{
        $formations = [];
        $sql = 'SELECT * FROM formation';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while($data_tmp = $request->fetchObject()) {
            $formations[] = $data_tmp;
        }
        return $formations;
    }

    /**
     * Summary of getAllName
     * @param string $orderby
     * @return array
     */
    public static function getAllName(string $orderby = ''): array
    {
        $formationsName = [];
        $sql = 'SELECT `name` 
                FROM formation';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $formationsName[] = $data_tmp;
        }
        return $formationsName;
    }
}