<?php

namespace app\Models;

class Groups extends Model{
    

    /**
     * Summary of createGroup
     * @param array $data
     * @return int
     */
    public static function createGroup(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO groups (name)
        VALUES (?)');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of updateGroup
     * @param int $id
     * @return bool
     */
    public static function updateGroup(int $id): bool{
        $request = self::$connect->prepare(
            "INSERT INTO groups (name) VALUES (?) WHERE id = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of delete
     * @param int $id
     * @return bool
     */
    public static function delete(int $id): bool
    {
        $request = self::$connect->prepare("DELETE FROM groups  WHERE id = ?");
        $request->execute([$id]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of AddUser
     * @param array $data
     * @return int
     */
    public static function AddUser(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO user_group (userid, groupid, created)
        VALUES (?, ?, NOW())');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of removeUser
     * @param array $data
     * @return bool
     */
    public static function removeUser(array $data): bool
    {
        $request = self::$connect->prepare("DELETE FROM user_group  WHERE groupid = ? AND userid = ?");
        $request->execute(array_values($data));
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of getAll
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array{
        $groups = [];
        $sql = 'SELECT * FROM `groups`';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while($data_tmp = $request->fetchObject()) {
            $groups[] = $data_tmp;
        }
        return $groups;
    }

    /**
     * Summary of getAllName
     * @param string $orderby
     * @return array
     */
    public static function getAllName(string $orderby = ''): array
    {
        $groupName = [];
        $sql = 'SELECT `name` 
                FROM groups';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $groupName[] = $data_tmp;
        }
        return $groupName;
    }

    /**
     * Summary of getByGroupName
     * @param string $name
     * @return int
     */
    public static function getByGroupName(string $name): int{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM groups WHERE name = ?");
        $result->execute([$name, ]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getGroupIdByName
     * @param string $name
     * @return int
     */
    public static function getGroupIdByName(string $name): int{
        $result = self::$connect->prepare("SELECT id FROM groups WHERE `name` = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getUserGroupIdByName
     * @param string $name
     * @return int
     */
    public static function getUserGroupIdByName(string $name): int{
        $result = self::$connect->prepare("SELECT ug.id FROM user_group ug
                                            JOIN groups g ON ug.groupid = g.id
                                            WHERE g.name = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getUserGroupIdByGroupId
     * @param string $name
     * @return int
     */
    public static function getUserGroupIdByGroupId(string $name): int{
        $result = self::$connect->prepare("SELECT ug.id FROM user_group ug
                                            JOIN groups g ON ug.groupid = g.id
                                            WHERE g.id = ?");
        $result->execute([$name]);
        return $result->fetchColumn();
    }

    /**
     * Summary of getUserEnrol
     * @param int $groupid
     * @return array
     */
    public function getUserEnrol(int $groupid): array
    {
        $users = [];
        $sql = 'SELECT g.name as group_name,
                u.username as user_name
                FROM user_group ug
                JOIN groups g ON ug.groupid = g.id AND g.id = ?
                JOIN user u ON u.id = ug.userid
                ORDER BY g.name, u.username';

        $request = self::$connect->prepare($sql);
        $request->execute([$groupid]);
        while ($data_tmp = $request->fetchObject()) {
            $users[] = $data_tmp;
        }
        return $users;
    }

    /**
     * Summary of getUserEnrolName
     * @param int $groupid
     * @return array
     */
    public function getUserEnrolName(int $groupid): array
    {
        $users = [];
        $sql = 'SELECT u.username as user_name
                FROM user_group ug
                JOIN groups g ON ug.groupid = g.id AND g.id = ?
                JOIN user u ON u.id = ug.userid
                ORDER BY g.name, u.username';

        $request = self::$connect->prepare($sql);
        $request->execute([$groupid]);
        while ($data_tmp = $request->fetchObject()) {
            $users[] = $data_tmp;
        }
        return $users;
    }

    /**
     * Summary of getByFormationForForm
     * @param int $formationid
     * @return array
     */
    public static function getByFormationForForm(int $formationid): array
    {
        $groups = [];
        $sql = 'SELECT g.id as groupid, 
                g.name as groupname
                FROM user_group_formation ugf
                JOIN formation f ON ugf.formationid = f.id
                JOIN user_group ug ON ugf.user_group_id = ug.id
                JOIN groups g ON ug.groupid = g.id
                WHERE f.id = ?
                ORDER BY g.name';

        $request = self::$connect->prepare($sql);
        $request->execute([$formationid]);
        while ($data_tmp = $request->fetchObject()) {
            $groups[$data_tmp->groupid] = $data_tmp->groupname;
        }
        return $groups;
    }

    /**
     * Summary of hasUser
     * @param int $groupid
     * @param int $userid
     * @return mixed
     */
    public static function hasUser(int $groupid, int $userid): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_group WHERE groupid = ? AND userid = ?");
        $result->execute([$groupid, $userid]);
        return $result->fetchColumn();
    }

    /**
     * Summary of AssignFormation
     * @param array $data
     * @return int
     */
    public static function AssignFormation(array $data): int{
        $insert = self::$connect->prepare('INSERT INTO user_group_formation (user_group_id, formationid, created)
        VALUES (?, ?, NOW())');
        $insert->execute(array_values($data));
        if($insert->rowCount()){
            return self::$connect->lastInsertId();
        }
        return 0;
    }

    /**
     * Summary of unassignFormation
     * @param array $data
     * @return bool
     */
    public static function unassignFormation(array $data): bool
    {
        $request = self::$connect->prepare("DELETE FROM user_group_formation  WHERE formationid = ? AND user_group_id = ?");
        $request->execute(array_values($data));
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * Summary of hasAssignFormation
     * @param int $formationid
     * @param int $user_groupid
     * @return mixed
     */
    public static function hasAssignFormation(int $formationid, int $user_groupid): mixed{
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_group_formation WHERE formationid = ? AND user_group_id = ?");
        $result->execute([$formationid, $user_groupid]);
        return $result->fetchColumn();
    }
}