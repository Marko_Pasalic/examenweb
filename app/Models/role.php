<?php

namespace app\Models;

class Role extends Model
{

    /**
     * Summary of getAll
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array
    {
        $roles = [];
        $sql = 'SELECT name FROM role';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $roles[] = $data_tmp;
        }
        return $roles;
    }
}
