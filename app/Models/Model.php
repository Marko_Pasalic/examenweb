<?php

namespace app\Models;

use PDO;
use app\Helpers\DB;

abstract class Model
{
    protected static PDO $connect;

    /**
     * Summary of __construct
     */
    public function __construct()
    {
        global $connect;

        if (!$connect) {
            $connect = DB::connect();
        }
        self::$connect = $connect;
    }

    /**
     * Summary of get
     * @param int $id
     * @return bool
     */
    public static function get(int $id): bool
    {
        return false;
    }

    /**
     * @param string $orderby
     * @return array
     */
    public static function getAll(string $orderby = ''): array
    {
        $datas = [];
        $sql = "SELECT * FROM " . self::getClassName();
        if ($orderby && in_array($orderby, self::getColumns())) {
            $sql .= " ORDER BY " . $orderby;
        }
        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $datas[] = $data_tmp;
        }
        return $datas;
    }

    /**
     * @param string $orderby
     * @param string $key
     * @param string $caption
     * @return array
     */
    public static function getAllForForm(string $orderby = '', string $key = 'id', string $caption = 'name'): array
    {
        $datas = [];
        $sql = "SELECT * FROM " . self::getClassName();
        if ($orderby && in_array($orderby, self::getColumns())) {
            $sql .= " ORDER BY " . $orderby;
        }
        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $datas[$data_tmp->$key] = $data_tmp->$caption;
        }
        return $datas;
    }

    /**
     *
     * @param string $field
     * @param string $value
     * @return mixed
     */
    public static function getByField(string $field, string $value): mixed
    {
        $request = self::$connect->prepare("SELECT * FROM " . self::getClassName() . " WHERE $field = ?");
        $request->execute([$value]);
        return $request->fetchObject();
    }

    /**
     *
     * @param string $field
     * @param string $value
     * @param int $id
     * @return mixed
     */
    public static function updateFieldById(string $field, string $value, int $id): mixed
    {

        // Si le champ ne fait pas partie de la liste des champs de la table en DB, la fonction retourne false
        if (!in_array($field, self::getColumns())) {
            return false;
        }

        // Cas spécial : dans le cas où NOW() est utilisé, il est inséré tel quel
        if ($value != 'NOW()') {
            $val = '?';
            $params = [$value, $id];
        } else {
            $val = $value;
            $params = [$id];
        }

        $request = self::$connect->prepare("UPDATE " . self::getClassName() . " SET $field = $val WHERE id = ?");
        $request->execute($params);
        // Retourne le nombre de records (lignes, rows) modifiés par la requête
        return $request->rowCount();
    }

    /**
     * Summary of create
     * @param array $data
     * @return int
     */
    public static function create(array $data): int
    {
        return 0;
    }

    /**
     * @param object $object
     * @return int
     */
    public static function update(object $object): int
    {
        if (empty($object->id)) {
            return false;
        }
        $params = array_values(get_object_vars($object));
        $params[] = $object->id;
        $setFields = self::getSelectFields($object, ',', ' = ?');
        $query = "UPDATE " . self::getClassName() . " SET $setFields WHERE id = ?";
        $request = self::$connect->prepare($query);
        $request->execute($params);
        return $request->rowCount();
    }

    /**
     * @param string $sql
     * @param array $params
     * @param string $fetch
     * @return mixed
     */
    public static function raw(string $sql, array $params = [], string $fetch = 'RESULT'): mixed
    {
        $result = self::$connect->prepare($sql);
        $result->execute($params);
        if ($fetch == 'OBJECT') {
            $data = $result->fetchObject();
        } elseif ($fetch == 'UPDATE') {
            $data = $result->rowCount();
        } elseif ($fetch == 'COUNT') {
            $data = $result->fetchColumn();
        } elseif ($fetch == 'ALL') {
            $data = $result->fetchAll();
        } elseif ($fetch == 'MULTI') {
            $data = [];
            while ($data_tmp = $result->fetchObject()) {
                $data[] = $data_tmp;
            }
        } elseif ($fetch == 'INDEX') {
            $data = [];
            while ($data_tmp = $result->fetchObject()) {
                $data[$data_tmp->id] = $data_tmp;
            }
        } elseif ($fetch == 'IMPLODE') {
            $data_implode = [];
            while ($data_tmp = $result->fetchObject()) {
                $data_implode[] = $data_tmp->ID;
            }
            $data = implode(',', $data_implode);
        } else {
            $data = $result;
        }
        return $data;
    }

    /**

     *
     * @return string
     */
    protected static function getClassName(): string
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        return strtolower(end($data));
    }

    /**
     *
     * @return array
     */
    protected static function getColumns()
    {
        $columns = [];
        $cols = self::$connect->query("DESCRIBE " . self::getClassName(), PDO::FETCH_OBJ);
        foreach ($cols as $col) {
            $columns[] = $col->Field;
        }
        return $columns;
    }

    /**
     *
     * @param object $fields
     * @param string $concat
     * @param string $addtxt
     * @return string
     */
    protected static function getSelectFields(object $fields, string $concat, string $addtxt = ''): string
    {
        $return = '';
        $nbr = 0;
        foreach ($fields as $key => $value) {
            if ($nbr > 0) {
                if ($value == 'NOW()') {
                    $return .= $concat . $key . '=NOW()';
                } else {
                    $return .= $concat . $key . $addtxt;
                }
            } else {
                $return .= $key . $addtxt;
            }
            $nbr++;
        }
        return $return;
    }
}
