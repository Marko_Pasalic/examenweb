<?php

namespace app\Helpers;

class Helper
{
    /**
     * @return string
     */
    public static function getMaxFileSizeHumanReadable(): string
    {
        return min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
    }

    /**
     * @param $size
     * @return float
     */
    public static function parseSizeFromInitFile($size): float
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }

    /**
     * @return float
     */
    public static function getMaxFileSize(): float
    {
        return min(self::parseSizeFromInitFile(ini_get('post_max_size')), self::parseSizeFromInitFile(ini_get('upload_max_filesize')));
    }
}
