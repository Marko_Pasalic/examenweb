<?php

namespace app\Helpers;

use app\Controllers\Course;
use app\Controllers\Group;
use app\Controllers\User;
use app\Models\Formation;
use stdClass;

class Bootstrap
{
    /**
     * Display of an HTML table based on an object or a data array
     *
     * @param array|object $data
     * @param array|object $cols
     * @param string $title
     * @param string $class
     * @return string
     */
    public static function table(array|object $data, array|object $cols, string $title = '', string $class = ''): string
    {
        $thead = '';
        $tbody = '';
        if (is_array($cols) || is_object($cols)) {
            foreach ($cols as $th) {
                $thead .= '<th>' . $th . '</th>';
            }
        } else {
            $thead = '<th>' . $cols . '</th>';
        }
        if (is_array($data) || is_object($data)) {
            foreach ($data as $sub) {
                $tbody .= '<tr>';
                if (is_array($sub) || is_object($sub)) {
                    foreach ($sub as $value) {
                        $tbody .= '<td>' . $value . '</td>';
                    }
                } else {
                    $tbody .= '<td>' . $sub . '</td>';
                }
                $tbody .= '</tr>';
            }
        } else {
            $tbody .= '<tr><td>' . $data . '</td></tr>';
        }

        return '<h2>' . $title . '</h2><table class="table table-striped ' . $class . '"><thead><tr>' . $thead . '</tr></thead><tbody>' . $tbody . '</tbody></table>';
    }

    /**
     * Retrieves options from an HTML form select based on an object
     *
     * @param object|array $options
     * @param string $selected
     * @return string
     */
    public static function getFormOptions(object|array $options, string $selected = ''): string
    {
        $output = '';
        foreach ($options as $key => $value) {
            if ($key == $selected) {
                $attr = 'selected';
            } else {
                $attr = '';
            }
            $output .= '<option value="' . $key . '" ' . $attr . '>' . $value . '</option>';
        }
        return $output;
    }

    /**
     * Summary of userDropdownModal
     * @param object|array $data
     * @return string
     */
    public static function userDropdownModal(object|array $data): string
    {
        $output = '"<option disabled selected value> -- select an option -- </option>"';
        if (!empty($data)) {
            foreach ($data as $row) {
                foreach ($row as $key => $value) {
                    if ($value) {
                        $output .= "<option value=\"" . $value . "\">" . $value . "</option>";
                    }
                }
            }
        }
        return $output;
    }

    /**
     * Summary of genericDropdownModal
     * @param object|array $data
     * @return string
     */
    public static function genericDropdownModal(object|array $data): string
    {
        $output = '';
        if (!empty($data)) {
            foreach ($data as $row) {
                foreach ($row as $key => $value) {
                    if ($value) {
                        $output .= "<option value=\"" . $value . "\">" . $value . "</option>";
                    }
                }
            }
        }
        return $output;
    }

    /**
     * user profile view
     *
     * @see Bootstrap::profile()
     * @see Output::render()
     * @param object $data
     * @param string $class
     * @return string
     */

    public static function profile(object $data, string $class = ''): string
    {
        $lang = new stdClass();
        $lang->en = 'English';
        $lang->fr = 'Français';

        $tbody = '';
        $tbody .= '<div class="container">
        <div class="main-body">
        <p></p>
              <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex flex-column align-items-center text-center">
                        <img src="' . $data->image . '?' . time() . '" alt="photo" class="d-block img-fluid" id="profile-photo">
                        <br><a href="index.php?view=api/user/exportImage/' . $_SESSION['userid'] . '" class="btn btn-sm btn-primary">Exporter l\'image</a>
                        <div class="mt-3">
                          <h2>'. $data->username.'</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card mt-3">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                        <h6 class="mb-0">'.Text::getString(['Created', 'Créer le']).'</h6>
                        <span class="text-secondary">' . $data->created . '</span>
                      </li>
                      <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                        <h6 class="mb-0">'.Text::getString(['Last login', 'Dernière connexion']).'</h6>
                        <span class="text-secondary">' . $data->lastlogin . '</span>
                      </li>
                      <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                        <h6 class="mb-0">'.Text::getString(['Last updated', 'Dernière mise à jour']).'</h6>
                        <span class="text-secondary">' . $data->updated . '</span>
                      </li>
                    </ul>
                    <a href="index.php?view=api/user/exportProfile/' . $_SESSION['userid'] . '" class="btn btn-sm btn-primary">Exporter profile</a>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="card mb-3">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Full name', 'Nom complet']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                          '. $data->firstname . " " . $data->lastname .'
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Email</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->email . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Birthdate', 'Date de naissance']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->birthdate . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Phonenumber', 'Tel.']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->phonenumber . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Language', 'Langue']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->lang . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Nationality', 'Nationalité']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->nationality . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Address', 'adresse']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->address . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Postal code', 'Code postal']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->postalcode . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">'.Text::getString(['Country', 'Pays']).'</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->countries . '
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Admin</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        ' . $data->admin . '
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row gutters-sm">
                    <div class="col-sm-12 mb-3">
                      <div class="card h-100">
                        <div class="card-body">
                        <hr>
                        <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('update') . '</a></h3>
                        <div class="collapse" id="profile-update-collapse">
                           <form action="index.php?view=api/user/update" method="post" enctype="multipart/form-data">
                               <label for="update-password">' . Text::getStringFromKey('password') . '</label>
                               <input type="password" id="update-password" name="password" class="form-control" placeholder="Pour changer votre mot de passe, cliquez ici">
                               <label for="update-email">' . Text::getStringFromKey('email') . '</label>
                               <input type="email" id="update-email" name="email" class="form-control" value="' . $data->email . '">
                               <label for="update-lang">' . Text::getStringFromKey('lang') . '</label>
                               <select name="lang" id="uu-lang" class="form-control">
                                   ' . self::getFormOptions($lang, $data->lang) . '
                               </select>
                               <label for="update-address">' . Text::getString(['address', 'adresse']) . '</label>
                               <input type="text" id="update-address" name="address" class="form-control" value="' . $data->address . '">
                               <label for="countriesList">'.Text::getString(['Choose a country', 'Choisir un pays']).'</label>
                               <select name="countries" id="countries" class="form-control">
                                   <option value="">'.Text::getString(['Choose country', 'Choisissez un pays']).'</option>
                               </select>
                               <label for="update-postalcode">' . Text::getString(['Postalcode', 'Code postale']) . '</label>
                               <input type="postalcode" id="update-postalcode" name="postalcode" class="form-control" value="' . $data->postalcode . '">
                               <label for="update-photo">' . Text::getStringFromKey('photo') . '</label>
                               <p><input type="file" id="update-photo" name="photo"></p>
                               <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                           </form>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>';
        return '<table class="table ' . $class . '">' . $tbody . '</table>';
    }

    /**
     * @param object $data
     * @return void
     */
    // public static function exportProfile(object $data): void
    // {
    //     $filename = $data->username . '_' . 'Profile' . '_' . date('d-m-y h:i:s') . '.' . $data->format;
    //     header('Content-type: application/json');
    //     header('Content-disposition: attachment; filename="' . $filename . '"');

    //     if ($data->format == 'json') {
    //         unset($data->format);
    //         echo json_encode($data);
    //     } else {
    //         unset($data->format);
    //         foreach ($data as $key => $value) {
    //             echo $key . ' : ' . $value . "\n";
    //         }
    //     }
    // }

    public static function exportProfile(object $data): void
    {
        $filename = $data->username . '_' . time() . '.' . $data->format;
        $directory = ROOT_PATH . DIRECTORY_SEPARATOR . 'doc/export';
        if(!file_exists($directory)) mkdir($directory, 0700, true);
        $path = $directory . '/' . $filename;
        if ($data->format == 'json') {
            unset($data->format);
            $j = json_encode($data, JSON_PRETTY_PRINT);
            if(file_put_contents($path, $j)) Output::render('messageBox', 'Export effectué avec succès. Fichier disponible dans docs/export.', 'success');
            else echo 'Export échoué';
        } else {
            unset($data->format);
            foreach ($data as $key => $value) {
                echo $key .' : '. $value . "\n";
            }
        }
    }

    /**
     * @param object $data
     * @return void
     */
    public static function exportImage(object $data): void
    {
        $path = ROOT_PATH . DIRECTORY_SEPARATOR . $data->image;

        header('Content-type: image');
        header('Content-disposition: attachment; filename="' . $data->image . '"');
        echo file_get_contents($path);
    }

    /**
     * @param array $data
     * @return void
     */
    public static function exportCourseList(array $data): void
    {
        $data = json_decode(json_encode($data), true);
        $filename = 'courses_list_' . time() . '.csv';

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: no-store');
        $buffer = fopen('php://output', 'r+');
        foreach ($data as $course) {
            fputcsv($buffer, $course);
        }
        fclose($buffer);
    }

    /**
     * @param array $courses
     * @return string
     */
    public static function profileCourses(array $courses): string
    {
        $body = '';
        if (!empty($_SESSION['userid'])) {
            $exportlink = '<a href="index.php?view=api/user/exportCourseList/' . $_SESSION['userid'] . '" class="btn btn-sm btn-primary">'.Text::getString(['Export', 'Exporter']).'</a>';
        } else {
            $exportlink = '';
        }
        foreach ($courses as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {
                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
            $body .= '</tr>';
        }
        return '<hr><h2><a href="#profile-courses-list" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('courses') . '</a></h2>
                <table class="table table-striped collapse" id="profile-courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['degree', 'cycle']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                            <th>' . Text::getString(['role', 'rôle']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $exportlink;
    }

    /**
     * @link http://localhost/examenweb/js/main.js
     * @param array $data
     * @return string
     */
    public static function courses(array $data): string
    {
        $body = '';
        $modal = '';

        foreach ($data as $row) {
            $course = new Course();
            $body .= '<tr>';

            foreach ($row as $key => $value) {

                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
            if (!$course->getEnrol($row->courseid, $_SESSION['userid'])) {
                $body .= '<td><a href="index.php?view=api/course/enrol/' . $row->courseid . '/' . $_SESSION['userid'] . '" class="btn btn-sm btn-success">+</a></td>';
            } else {
                $body .= '<td>'.Text::getString(['Registered', 'Déjà inscrit']). '</td>';
            }
            $body .= '</tr>';
        }
        return '<h2>' . Text::getStringFromKey('coursesList') . '</h2>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['degree', 'cycle']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                            <th>' . Text::getString(['enrol', 'inscrire']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;
    }

    /**
     * Summary of coursesListAdmin
     * @param array $data
     * @return string
     */
    public static function coursesListAdmin(array $data): string
    {
        $body = '';
        $modal = '';

        foreach ($data as $row) {
            $course = new Course();
            $body .= '<tr>';

            foreach ($row as $key => $value) {

                $body .= '<td>' . $value . '</td>';
            }
            $body .= '<td>' . '<input type="button" class="btn btn-primary" onclick="updateCourse(this);" value="' . Text::getString(['Update', 'Modifier']) . '">' . '</td>'
                . '<td>' . '<input type="button" class="btn btn-danger" onclick="deleteCourse(this);" value="' . Text::getString(['Delete', 'Supprimer']) . '">' . '</td>';
            $body .= '</tr>';
        }
        return '<h2>' . Text::getStringFromKey('courses') . '</h2>
        <input type="button" class="btn btn-success btn-lg" onclick="createCourse(this);"  value="' . Text::getString(['+ Create a course', '+ Créer un cours']) . '">
            <p></p>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="courses-list">
                    <thead>
                        <tr>
                        <th>id</th>
                            <th>' . Text::getString(['course name', 'nom du cours']) . '</th>
                            <th>' . Text::getString(['code', 'code']) . '</th>
                            <th>' . Text::getString(['status', 'statut']) . '</th>
                            <th>' . Text::getStringFromKey('update') . '</th>
                            <th>' . Text::getStringFromKey('delete') . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;
    }

    /**
     * Summary of courseUpdate
     * @param object $data
     * @return string
     */
    public static function courseUpdate(object $data): string
    {
        return '<hr>
                <h1>' . Text::getString(['update course', 'mettre à  jour le cours']) . '</h1>
                    <form action="index.php?view=api/course/update" method="post">
                        <input type="hidden" id="uu-course" name="id" value="' . $data->id . '">
                        <label for="update-name">' . Text::getString(['Course name', 'Nom du cours']) . '</label>
                        <input type="text" id="update-name" name="name" value="' . $data->name . '" class="form-control" required>
                        <p></p>
                        <label for="update-code">Code</label>
                        <input type="number" id="update-code" name="update-code" value"' . $data->code . '" min="1000" max="9999">
                        <p></p>
                        <label for="update-status">' . Text::getString(['Choose a status', 'Choisir un statut']) . '</label>
                        <select name="status" id="update-status" class="form-control" required>
                            <option value="active" selected>' . Text::getString(['active', 'actif']) . '</option>
                            <option value="inactive">' . Text::getString(['inactive', 'inactif']) . '</option>
                        </select>
                        <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                    </form>';
    }

    /**
     * Summary of unassignCourse
     * @param array $data
     * @return string
     */
    public static function unassignCourse(array $data)
    {
        include_once ROOT_PATH . '/view/admin/menu.html';
        
        return '<form action="index.php?view=api/formation/unassignCourse" method="post">
                <label for="formation-name">' . Text::getString(['Formation name', 'Nom de la formation']) . '</label>
                    <select name="formation-name" id="formation-name" class="form-control" required>
                    <option></option>
                    ' . self::getFormOptions($data) . '
                    </select>
                <p></p>
                <label for="course-name">' . Text::getString(['Course name', 'Nom du cours']) . '</label>
                    <select name="course-name" id="course-name" class="form-control" required>
                        <option></option>
                    </select>
                <br>
                <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                </form>';
    }
        
    /**
     * View for formation list
     * @param array $data
     * @return string
     */
    public static function formations(array $data): string
    {
        $body = '';
        $modal_footer = '<div class="d-none">
                        <span id="m-uid">' . $_SESSION['userid'] . '</span>
                        <span id="m-tokenu">' . Access::generateToken('modal-formation-token', $_SESSION['userid']) . '</span>
                        </div>';
        $modal = self::viewModal('modal-formation-update', Text::getString(['formation update', 'mettre à jour la formation']), '', $modal_footer, 'lg');

        foreach ($data as $row) {
            $body .= '<tr>';

            foreach ($row as $key => $value) {

                if ($key == 'begindate') {
                    if ($_SESSION['lang'] == 'fr') {
                        $value = date('d-m-Y', strtotime($value ?? ''));
                    } else {
                        $value = date('m-d-Y', strtotime($value ?? ''));
                    }
                }

                if ($key == 'enddate') {
                    if ($_SESSION['lang'] == 'fr') {
                        $value = date('d-m-Y', strtotime($value ?? ''));
                    } else {
                        $value = date('m-d-Y', strtotime($value ?? ''));
                    }
                }
                $body .= '<td>' . $value . '</td>';
            }
            $body .=
                '<td>' . '<input type="button" class="btn btn-primary" onclick="updateFormation(this);" value="' . Text::getString(['Update', 'Modifier']) . '">' . '</td>'
                . '<td>' . '<input type="button" class="btn btn-danger" onclick="deleteFormation(this);" value="' . Text::getString(['Delete', 'Supprimer']) . '">' . '</td>';
            $body .= '</tr>';
        }
        return '<h2>' . Text::getStringFromKey('formation') . '</h2>
                <input type="button" class="btn btn-success btn-lg" onclick="createFormation(this);"  value="' . Text::getString(['+ Create a formation', '+ Créer une formation']) . '">
                <input type="button" class="btn btn-warning btn-lg" onclick="assignCourseBtn(this);"  value="' . Text::getString(['Assign a course', 'Assigner un cours']) . '">
                <input type="button" class="btn btn-danger btn-lg" onclick="unassignCourseBtn(this);"  value="' . Text::getString(['Unassign a course', 'Désassigner un cours']) . '">
                <p></p>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="formation-list">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>' . Text::getString(['name', 'nom']) . '</th>
                            <th>' . Text::getString(['degree', 'cycle']) . '</th>
                            <th>' . Text::getString(['status', 'statut']) . '</th>
                            <th>' . Text::getString(['begindate', 'date de début']) . '</th>
                            <th>' . Text::getString(['enddate', 'date de fin']) . '</th>
                            <th>' . Text::getStringFromKey('update') . '</th>
                            <th>' . Text::getStringFromKey('delete') . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;
    }

    /**
     * Summary of formationUpdate
     * @param object $data
     * @return string
     */
    public static function formationUpdate(object $data): string
    {
        $degree = new stdClass();
        $degree->MASTER = 'Master';
        $degree->Bac = 'Bac';
        $degree->BES = 'BES';
        $degree->DS = 'DS';
        $degree->DI = 'DI';

        return '<hr>
                <h1>' . Text::getString(['update formation', 'mettre à  jour la formation']) . '</h1>
                    <form action="index.php?view=api/formation/update" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="uu-formationid" name="id" value="' . $data->id . '">
                        <label for="update-name">' . Text::getString(['Formatin name', 'Nom de la formation']) . '</label>
                        <input type="text" id="update-name" name="name" value="' . $data->name . '" class="form-control" required>
                        <label for="update-degree">' . Text::getString(['Choose a degree', 'Choisir un cycle']) . '</label>
                        <select name="degree" id="update-degree" class="form-control" required>
                        ' . self::getFormOptions($degree, $data->degree) . '
                        </select>
                        <label for="update-status">' . Text::getString(['Choose a status', 'Choisir un statut']) . '</label>
                        <select name="status" id="update-status" class="form-control" required>
                            <option value="active" selected>' . Text::getString(['active', 'actif']) . '</option>
                            <option value="inactive">' . Text::getString(['inactive', 'inactif']) . '</option>
                        </select>
                        <label for="update-begindate">' . Text::getString(['Begin date', 'Date de début']) . '</label>
                        <input type="date" id="update-begindate" name="begindate" value="' . $data->begindate . '" class="form-control">
                        <label for="update-enddate">' . Text::getString(['End date', 'Date de fin']) . '</label>
                        <input type="date" id="update-enddate" name="enddate" value="' . $data->enddate . '" class="form-control">
                        <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                    </form>';
    }

    /**
     * @param array $data
     * @return string
     */
    public static function users(array $data): string
    {
        Access::checkAdmin();
        $body = '';
        $modal_footer = '<div class="d-none">
                             <span id="m-uid">' . $_SESSION['userid'] . '</span>
                             <span id="m-tokenu">' . Access::generateToken('modal-user-token', $_SESSION['userid']) . '</span>
                         </div>';

        $modal = self::viewModal('modal-user-list', Text::getString(['user detail', 'détail utilisateur']), '', $modal_footer, 'lg');
        foreach ($data as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {
                if ($key == 'username') {
                    $value = self::linkModal('modal-user-list', $value , 'user-modal-link') . '';
                } elseif ($key == 'image') {
                    $value = '';
                }
                if ($key == 'rolename') {
                    $value = '  
                                <select name="user_role" id="user_role"  onclick="roleSelector(this);" onchange="getSelectedOption(this);">
                                <option disabled selected value value="">' . $value . '</option>
                                </select>'
                        . '</td>';
                }
                if ($value) {
                    $body .= '<td>' . $value . '</td>';
                }
            }
        }
        include_once ROOT_PATH . '/view/admin/menu.html';
        return '<h2>' . Text::getStringFromKey('user', true, 2, 's') . '</h2>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="users-list">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>' . Text::getStringFromKey('username') . '</th>
                            <th>' . Text::getString(['email', 'email']) . '</th>
                            <th>' . Text::getStringFromKey('created') . '</th>
                            <th>' . Text::getStringFromKey('lastlogin') . '</th>
                            <th>' . Text::getStringFromKey('lang') . '</th>
                            <th>' . Text::getStringFromKey('role') . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;
    }

    /**
     * @param object $data
     */
    public static function usermodal(object $data): void
    {
        $output = '';
        include_once ROOT_PATH . '/view/footer.html';
        if (!empty($data->user->image)) {
            $output .= '<div class="row">
                            <div class="col-6">
                                <img src="' . $data->user->image . '?' . time() . '" alt="photo" class="d-block img-fluid">
                            </div>
                          </div>';
        }
        if (!empty($data->courseslist)) {
            $output .= $data->courseslist;
        }
        echo $output;
    }

/**
 * Summary of groups
 * @param array $data
 * @return string
 */
public static function groups(array $data): string
{
    $body = '';
    $modal_footer = '<div class="d-none">
                        <span id="m-uid">' . $_SESSION['userid'] . '</span>
                        <span id="m-tokenu">' . Access::generateToken('modal-user-token', $_SESSION['userid']) . '</span>
                    </div>';
    $modal = self::viewModal('modal-group-user-list', Text::getString(['group detail', 'détail du groupe']), '', $modal_footer, 'lg');
    foreach ($data as $row) {
        $body .= '<tr>';

        foreach ($row as $key => $value) {

            if($key == 'name'){
                $value = self::linkModal('modal-group-user-list', $value, 'group-user-modal-link');
            }
            $body .= '<td>' . $value . '</td>';
        }
        $body .= '<td>' . '<input type="button" class="btn btn-primary" onclick="updateGroup(this);" value="' . Text::getString(['Update', 'Modifier']) . '">' . '</td>'
            . '<td>' . '<input type="button" class="btn btn-info" onclick="addUserBtn(this);" value="' . Text::getString(['Add user', 'Ajouter utilisateur']) . '">' . '</td>'
            . '<td>' . '<input type="button" class="btn btn-warning" onclick="removeUserBtn(this);" value="' . Text::getString(['Remove user', 'Supprimer utilisateur']) . '">' . '</td>'
            . '<td>' . '<input type="button" class="btn btn-danger" onclick="deleteGroup(this);" value="' . Text::getString(['Delete', 'Supprimer']) . '">' . '</td>';
        $body .= '</tr>';
    }
    include_once ROOT_PATH . '/view/admin/menu.html';
    return '<h2>' . Text::getStringFromKey('groups') . '</h2>
            <input type="button" class="btn btn-success btn-lg" onclick="createGroup(this);"  value="' . Text::getString(['Create a group', '+ Créer un groupe']) . '">
            <input type="button" class="btn btn-warning btn-lg" onclick="assignGroupBtn(this);"  value="' . Text::getString(['Assign to a formation', 'Assigner à une formation']) . '">
            <input type="button" class="btn btn-danger btn-lg" onclick="unassignFormationForm(this);"  value="' . Text::getString(['Unassign from formation', 'Désassigner d\'une formation']) . '">
            <p></p>
            <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="groups-list">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>' . Text::getString(['Name', 'Nom']) . '</th>
                        <th>' . Text::getStringFromKey('update') . '</th>
                        <th>' . Text::getString(['Add user', 'Ajouter utilisateur']) . '</th>
                        <th>' . Text::getString(['Remove user', 'Retirer utilisateur']) . '</th>
                        <th>' . Text::getStringFromKey('delete') . '</th>
                    </tr>
                </thead>
                <tbody>
                    ' . $body . '
                </tbody>
            </table>'. $modal;
}

/**
 * Summary of groupUpdate
 * @param object $data
 * @return string
 */
public static function groupUpdate(object $data): string
{
    return '<hr>
            <h1>' . Text::getString(['update group', 'mettre à  jour le groupe']) . '</h1>
                <form action="index.php?view=api/groups/update" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="update-groupid" name="id" value="' . $data->id . '">
                    <label for="update-name">' . Text::getString(['Group name', 'Nom du groupe']) . '</label>
                    <input type="text" id="update-name" name="update-name" value="' . $data->name . '" class="form-control" required>
                    <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                </form>';
}

/**
 * Summary of groupUserRemove
 * @param object $data
 * @return string
 */
public static function groupUserRemove(object $data): string
{
    return '<hr>
            <h1>' . Text::getString(['Manage users', 'Gérer les utilisateurs']) . '</h1>
                <form action="index.php?view=api/groups/removeUser" method="post"">
                    <input type="hidden" id="remove-groupid" name="remove-groupid" value="' . $data->id . '">
                    <label for="remove-user">' . Text::getString(['remove user', 'Retirer un Utilisateur']) .'</label>
                    <select name="remove-user" id="remove-user" onclick="getUsernameEnrol(this);" class="form-control" required>
                        <option></option>
                    </select>
                    <p></p>
                    <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                </form>';
}

/**
 * Form that allowed a group to unassign from a formation
 * @param array $data
 * @return string
 */
public static function unassignGroup(array $data)
{
    include_once ROOT_PATH . '/view/admin/menu.html';
    
    return '<form action="index.php?view=api/groups/unassignGroup" method="post">
            <label for="formation-name">' . Text::getString(['Formation name', 'Nom de la formation']) . '</label>
                <select name="formation-name" id="formation-name" class="form-control" required>
                <option></option>
                ' . self::getFormOptions($data) . '
                </select>
            <p></p>
            <label for="group-name">' . Text::getString(['Group name', 'Nom du groupe']) . '</label>
                <select name="group-name" id="group-name" class="form-control" required>
                    <option></option>
                </select>
            <br>
            <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
            </form>';
}

/**
 * Summary of groupAddUser
 * @param object $data
 * @return string
 */
public static function groupAddUser(object $data): string
{
    return '<hr>
            <h1>' . Text::getString(['Manage users', 'Gérer les utilisateurs']) . '</h1>
                <form action="index.php?view=api/groups/addUser" method="post"">
                    <input type="hidden" id="add-groupid" name="add-groupid" value="' . $data->id . '">
                    <label for="add-user">' . Text::getString(['add user', 'Ajouter un Utilisateur']) .'</label>
                    <select name="add-user" id="add-user" onclick="userNameForGroup(this);" class="form-control" required>
                        <option></option>
                    </select>
                    <p></p>
                    <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                </form>';
}

    /**
     * Summary of groupUserListModal
     * @param object $data
     * @return void
     */
    public static function groupUserListModal(object $data): void
    {
        include_once ROOT_PATH . '/view/footer.html';
        $output = '<div class="modal-content">';

        if (!empty($data->userslist)) {
            $output .= $data->userslist;
        }
        echo $output;
    }

    /**
     * Summary of groupUserList
     * @param array $data
     * @return string
     */
    public static function groupUserList(array $data): string
{
    $body = '';
    foreach ($data as $row) {
        $body .= '<tr>';
        foreach ($row as $key => $value) {
            $body .= '<td>' . $value . '</td>';
        }
        $body .= '</tr>';
    }
    return '<div class="modal-content">
    <h2>'. Text::getString(['Users List', 'Liste utilisateurs']) . '</h2>
    <table class="table table-striped table-dt" id="profile-group-user-list">
                <thead>
                    <tr>
                        <th>' . Text::getString(['Group name', 'Nom du groupe']) . '</th>
                        <th>' . Text::getString(['Username', 'Nom de l\'utilisateur']) . '</th>
                    </tr>
                </thead>
                <tbody>
                    ' . $body . '
                </tbody>
            </table>';
}
    /**
     * @param array $data
     * @return string
     */

    /**
     * Generic view for messages in a div using bootstrap alert class
     *
     * @see Output::render()
     * @param string $message
     * @param string $class
     * @return string
     */
    public static function messageBox(string $message, string $class = 'danger'): string
    {
        return '<div class="alert alert-' . $class . '">' . $message . '</div>';
    }

    /**
     * @param string $content
     * @param string $type
     * @param bool $dismiss
     * @return string
     */
    public static function alert(string $content, string $type = 'success', bool $dismiss = false): string
    {
        if ($dismiss) {
            return '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              ' . $content . '
            </div>';
        } else {
            return '<div class="alert alert-' . $type . '">' . $content . '</div>';
        }
    }

    /**
     * @param string $link
     * @param string $caption
     * @param string $class
     * @return string
     */
    public static function linkBtn(string $link, string $caption, string $class = 'default'): string
    {
        return '<a href="' . $link . '" class="btn btn-' . $class . '">' . $caption . '</a>';
    }

    /**
     * @param string $link
     * @param string $caption
     * @param string $target
     * @return string
     */
    public static function link(string $link, string $caption, string $target = 'default'): string
    {
        return '<a href="' . $link . '" class="lien" target="' . $target . '">' . $caption . '</a>';
    }

    /**
     *
     *
     * @param string $target_id
     * @param string $btn_text
     * @param string $btn_class
     * @return string
     */
    public static function btnModal(string $target_id, string $btn_text, string $btn_class = 'primary'): string
    {
        return '<button type="button" class="btn btn-' . $btn_class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $btn_text . '</button>';
    }

    /**
     * Link Modal bootstrap
     *
     * @param string $target_id
     * @param string $caption
     * @param string $class
     * @return string
     */
    public static function linkModal(string $target_id, string $caption, string $class = ''): string
    {
        return '<a href="#" class="link ' . $class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $caption . '</a>';
    }


    /**
     *
     *
     * @param string $modal_id
     * @param string $modal_title
     * @param string $modal_body
     * @param string $modal_footer
     * @param string $modal_size
     * @return string
     */
    public static function viewModal(string $modal_id, string $modal_title, string $modal_body, string $modal_footer = '', string $modal_size = ''): string
    {
        if ($modal_size == 'lg') {
            $modal_size = ' modal-lg';
        } elseif ($modal_size == 'hg') {
            $modal_size = ' modal-hg';
        }
        return '<div class="modal fade" tabindex="-1" id="' . $modal_id . '" aria-hidden="true">
          <div class="modal-dialog' . $modal_size . '">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">' . $modal_title . '</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body" id="' . $modal_id . '-body">
                ' . $modal_body . '
              </div>
              <div class="modal-footer">
                ' . $modal_footer . '
              </div>
            </div>
          </div>
        </div>';
    }

    /**
     * @param string $title
     * @param string $text
     * @param string $footer
     * @param string $style
     * @param string $class
     * @param string $id
     * @return string
     */
    public static function showCard(string $title, string $text, string $footer = '', string $style = '', string $class = '', string $id = ''): string
    {
        if ($footer) {
            $footer = '<div class="card-footer">' . $footer . '</div>';
        }
        return '<div class="card card-war ' . $class . '" style="' . $style . '" id="' . $id . '">
                <div class="card-header">' . $title . '</div>
                <div class="card-body">' . $text . '</div>
                ' . $footer . '
            </div>';
    }
}
