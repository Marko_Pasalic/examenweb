<?php

namespace app\Helpers;

use app\Controllers\Role;
use app\Controllers\User;

class Access
{
    /**
     * Function returning the boolean state of the session variable's userid element
     *
     * @return bool
     */
    public static function isLoggedIn(): bool
    {
        if (!empty($_SESSION['userid'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return void
     */
    public static function checkLoggedIn(): void
    {
        if (empty($_SESSION['userid'])) {
            header('Location: index.php?view=view/user/login');
            die;
        }
    }

    /**
     *
     * @param int $id
     * @return void
     */
    public static function checkProfile(int $id): void
    {
        if (empty($_SESSION['userid']) || $_SESSION['userid'] != $id) {
            header('Location: index.php?view=view/user/login');
            die;
        }
    }

    /**
     * @return void
     */
    public static function checkAdmin(): void
    {
        if (!self::isAdmin()) {
            self::redirect();
        }
    }

    /**
     * @return bool
     */
    public static function isAdmin(): bool
    {
        $user = new User();
        return $user->isAdmin($_SESSION['userid']);
    }

    /**
     * Summary of checkBanned
     * @param int $id
     * @return void
     */
    public static function checkBanned(int $id): void
    {
        if (self::isBanned($id)) {
            Output::createAlert(Text::getString(['This user account has been banned, please contact an administrator', 'Ce compte utilisateur a été banni, veuillez contacter un administrateur']), 'danger', 'index.php?view=/user/login');
        }
    }

    /**
     * Summary of isBanned
     * @param int $id
     * @return bool
     */
    public static function isBanned(int $id): bool
    {
        $user = new User();
        return $user->isBanned($id);
    }

    /**
     * @param int $roleid
     * @return void
     */
    public static function checkAccess(int $roleid): void
    {
        $user = new User();
        if (!$user->hasRole($_SESSION['userid'], $roleid)) {
            self::redirect();
        }
    }

    /**
     * Summary of generateToken
     * @param string $string
     * @param int $userid
     * @param bool $ts
     * @return string
     */
    public static function generateToken(string $string, int $userid, bool $ts = true): string
    {
        if ($ts) {
            $string = date('Y-m-d') . $string;
        }
        return hash_hmac('sha256', $string . '/' . $userid, HMAC_SALT);
    }

    /**
     * @param string $token
     * @param string $string_to_check
     * @param int $userid
     * @param bool $ts
     * @return bool
     */
    public static function checkToken(string $token, string $string_to_check, int $userid, bool $ts = true): bool
    {
        $token_check = self::generateToken($string_to_check, $userid, $ts);
        if (hash_equals($token_check, $token)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param string $location
     * @return void
     */
    public static function redirect(string $location = 'index.php'): void
    {
        header('Location: ' . $location);
        die;
    }
}
