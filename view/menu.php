<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Namur Cadet</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="index.php?view=view/default" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('home'); ?></a>
                </li>
                <?php if (!empty($_SESSION['userid'])) { ?>
                    <li class="nav-item">
                        <a href="index.php?view=api/course/list" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('coursesList'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?view=api/user/profile/<?= $_SESSION['userid'] ?>" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('profile'); ?></a>
                    </li>
                    <?php if (\app\Helpers\Access::isAdmin()) { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="index.php?view=view/admin/index" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Admin
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark">
                                <li><a class="dropdown-item" href="index.php?view=api/course/listAdmin"><?php echo \app\Helpers\Text::getStringFromKey('courses'); ?></a></li>
                                <li><a class="dropdown-item" href="index.php?view=api/formation/list"><?php echo \app\Helpers\Text::getStringFromKey('formations'); ?></a></li>
                                <li><a class="dropdown-item" href="index.php?view=api/user/usersList"><?php echo \app\Helpers\Text::getStringFromKey('users'); ?></a></li>
                                <li><a class="dropdown-item" href="index.php?view=api/groups/list"><?php echo \app\Helpers\Text::getStringFromKey('groups'); ?></a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a href="index.php?view=api/user/logout" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('logout'); ?></a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a href="index.php?view=view/user/login" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('login'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?view=view/user/registration" class="nav-link"><?php echo \app\Helpers\Text::getStringFromKey('registration'); ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>