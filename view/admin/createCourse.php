<?php use app\Helpers\Text;?>

<form action="index.php?view=api/course/createCourse" method="post">
    <label for="name"><?php echo Text::getString(['Course name', 'Nom du cours'])?></label>
    <input type="text" id="name" name="name" class="form-control" required>
    <label for="code">Code</label>
    <input type="number" name="code" min="1000" max="9999">
    <label for="status"><?php echo Text::getString(['Choose a status', 'Choisir un statut'])?></label>
    <select name="status" id="status" class="form-control" required>
        <option value="active" selected><?php echo Text::getString(['active', 'actif'])?></option>
        <option value="inactive"><?php echo Text::getString(['inactive', 'inactif'])?></option>
    </select>
    <input type="submit" class="btn btn-primary" value="Valider">
</form>