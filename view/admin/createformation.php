<?php use app\Helpers\Text;?>

<form action="index.php?view=api/formation/createFormation" method="post">
    <label for="name"><?php echo Text::getString(['Formation name', 'Nom de la formation'])?></label>
    <input type="text" id="name" name="name" class="form-control" required>
    <label for="degree"><?php echo Text::getString(['Choose a degree', 'Choisir un cycle'])?></label>
    <select name="degree" id="degree" class="form-control" required>
        <option value="MASTER">MASTER</option>
        <option value="Bac">BAC</option>
        <option value="BES">BES</option>
        <option value="DS">DS</option>
        <option value="DI">DI</option>
    </select>
    <label for="status"><?php echo Text::getString(['Choose a status', 'Choisir un statut'])?></label>
    <select name="status" id="status" class="form-control" required>
        <option value="active" selected><?php echo Text::getString(['active', 'actif'])?></option>
        <option value="inactive"><?php echo Text::getString(['inactive', 'inactif'])?></option>
    </select>
    <label for="begindate"><?php echo Text::getString(['begin date', 'date de début'])?></label>
    <input type="date" id="begindate" name="begindate" class="form-control">
    <label for="enddate"><?php echo Text::getString(['end date', 'date de fin'])?></label>
    <input type="date" id="enddate" name="enddate" class="form-control">
    <input type="submit" class="btn btn-primary" value="Valider">
</form>