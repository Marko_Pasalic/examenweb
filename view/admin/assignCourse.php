<form action="index.php?view=api/formation/assignCourse" method="post">
    <label for="formation-name"> <?php echo \app\Helpers\Text::getString(['Formation name', 'Nom de la formation']) ?> </label>
    <select name="formation-name" id="formation-name" onclick="FormationsName(this);" class="form-control" required>
        <option></option>
    </select>
    <p></p>
    <label for="course-name"> <?php echo \app\Helpers\Text::getString(['Course name', 'Nom du cours']) ?> </label>
    <select name="course-name" id="course-name" onclick="CoursesName(this);" class="form-control" required>
        <option></option>
    </select>
    <p></p>
    <label for="period"><?php echo \app\Helpers\Text::getString(['Period', 'Périodes']) ?></label>
    <input type="number" name="period" min="0" max="500" class="form-control" required>
    <label for="determinant"><?php echo \app\Helpers\Text::getString(['Determinant', 'Déterminant']) ?></label>
    <input type="checkbox" name="determinant" value="1" />
    <p></p>
    <label for="prerequisite"><?php echo \app\Helpers\Text::getString(['Prerequisite', 'Prérequis']) ?></label>
    <select name="prerequisite" id="prerequisite" onclick="CoursesName(this);" class="form-control">
        <option selected><?php echo \app\Helpers\Text::getString(['None', 'Aucun']) ?></option>
    </select>
    <label for="teacher"><?php echo \app\Helpers\Text::getString(['Teacher', 'Teacher']) ?></label>
    <select name="teacher" id="teacher" onclick="getAllTeacher(this);" class="form-control">
        <option></option>
    </select>
    <br>
    <input type="submit" class="btn btn-primary" value="<?php echo \app\Helpers\Text::getStringFromKey('submit') ?>">
</form>