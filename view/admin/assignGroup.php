<form action="index.php?view=api/groups/assignFormation" method="post">
    <label for="formation-name"><?php echo \app\Helpers\Text::getString(['Formation name', 'Nom de la formation']) ?></label>
    <select name="formation-name" id="formation-name" onclick="FormationsName(this);" class="form-control" required>
        <option></option>
    </select>
    <p></p>
    <label for="group-name"><?php echo \app\Helpers\Text::getString(['Group name', 'Nom du groupe']) ?></label>
    <select name="group-name" id="group-name" onclick="groupName(this);" class="form-control" required>
        <option></option>
    </select>
    <br>
    <input type="submit" class="btn btn-primary" value="<?php echo \app\Helpers\Text::getStringFromKey('submit') ?>">
</form>