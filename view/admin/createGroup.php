<form action="index.php?view=api/groups/createGroup" method="post">
    <label for="name"><?php echo \app\Helpers\Text::getString(['Group name', 'Nom du groupe']) ?></label>
    <input type="text" id="name" name="name" class="form-control" required>
    <p></p>
    <input type="submit" class="btn btn-primary" value="Valider">
</form>