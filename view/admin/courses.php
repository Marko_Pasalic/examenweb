<?php

use app\Helpers\Text; ?>

<div class="include-admin-menu"></div>
<h2><?php echo Text::getString(['Formations and courses management', 'Gestion des formations et des cours']) ?></h2>
<ul>
  <li>
    <a href="index.php?view=api/formation/list">
      <?php echo Text::getString(['Manage formations', 'Gérer les formations']) ?>
    </a>
  </li>
  <li>
    <a href="index.php?view=api/course/listAdmin">
      <?php echo Text::getString(['Manage courses', 'Gérer les cours']) ?>
    </a>
  </li>
  </a>
  </li>
</ul>