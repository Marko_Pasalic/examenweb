<main role="main" class="container-fluid">
<?php

use app\Helpers\Output;
use app\Helpers\Text;


Output::manageAlerts();

if (!empty($_GET['view']) && $_GET['view'] != 'view/default') {
    try {
      Output::getContent($_GET['view']);
  } catch (Exception $e) {
      Output::render('messageBox', Text::getString(['Veuillez utiliser l\'interface du site', 'Please use site interface']));
  }

} else {
    echo(
        '<main class="my-5">
          <div class="container">
            <section>
              <div id="intro" class="p-5 text-center bg-light shadow-5 rounded-5 mb-5">
                <h1 class="mb-3 h2">'. Text::getStringFromKey('welcome') .'</h1>
                <p class="mb-3">'. Text::getString(['Adult education and continuing education', 'Enseignement pour adulte et formation continue']) .'</p>
              </div>
            </section>
            <section class="text-center">
              <h4 class="mb-5"><strong>'. Text::getString(['Latest news', 'Dernières nouveautés']) .'</strong></h4>
      
              <div class="row">
                <div class="col-lg-4 col-md-12 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/BacInfoGestion.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">'. Text::getString(['Management IT', 'Informatique de gestion']) .'</h5>
                      <p class="card-text">
                      '. Text::getString(['Bachelor of Management Information Technology', 'Bachelier Informatique de gestion']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/webdev.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">'. Text::getString(['Web development', 'Web developement']) .'</h5>
                      <p class="card-text">
                      '. Text::getString(['Backend, frontend, design and website creation', 'Backend, frontend, design et création de site']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/fleuriste.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">'. Text::getString(['Floral Art', 'Art floral']) .'</h5>
                      <p class="card-text">
                      '. Text::getString(['Study of the different forms of composition: compositions of natural flowers...', 'Étude des différentes formes de composition : compositions de fleurs naturelles...']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
              </div>
      
              <div class="row">
                <div class="col-lg-4 col-md-12 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/cisco.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">Cisco</h5>
                      <p class="card-text">
                      '. Text::getString(['EAFC Namur Cadets has been a Local Cisco Academy since 2007', 'L’EAFC Namur Cadets est Académie Locale Cisco depuis 2007 ']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/ebusiness.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">E-business</h5>
                      <p class="card-text">
                      '. Text::getString(['The bachelor in e-business is a professional in management and marketing relations', 'Le bachelier en e-business est un professionnel du management et des relations marketing']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img src="../image/homepage/cap.jpeg" class="img-fluid" />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">CAP</h5>
                      <p class="card-text">
                      '. Text::getString(['You want to teach, You do not have a ruler or aggregation...', 'Vous désirez enseigner, Vous ne possédez pas de régendat ou d\'agrégation...']) .'
                      </p>
                      <a href="#!" class="btn btn-primary">Read</a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <nav class="my-4" aria-label="...">
              <ul class="pagination pagination-circle justify-content-center">
                <li class="page-item">
                  <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active" aria-current="page">
                  <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#">Next</a>
                </li>
              </ul>
            </nav>
          </div>
        </main> 
        <footer class="bg-light text-lg-start">
          <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2023 Marko Pasalic
          </div>
        </footer>'
);
}
?>
</main>