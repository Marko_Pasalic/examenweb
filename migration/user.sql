CREATE TABLE `user` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `lastname` varchar(255) NOT NULL,
    `firstname` varchar(255) NOT NULL,
    `nationality` varchar(25) NULL,
    `lang` varchar(5) NULL,
    `birthdate` date NULL,
    `address` varchar(120) NULL,
    `countries` varchar(120) NULL,
    `postalcode` int(5) NULL,
    `phonenumber` varchar(12) NULL,
    `image` varchar(255) NULL,
    `created` datetime NOT NULL,
    `lastlogin` datetime NOT NULL,
    `updated` datetime NULL,
    `admin` tinyint(1) DEFAULT NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `username` (`username`)
)   ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `username`, `password`, `email`, `lastname`, `firstname`, `lang`, `created`, `lastlogin`, `admin`) VALUES
    (1, 'ROOT', '$2y$10$nb1bJF8xneRH9waoUfaMJOBTLjn/u8H7LywURB68eQCk/tIm2TdMG', 'ROOT@gmail.com', 'ROOT', 'ROOT', 'en', NOW(), NOW(), 1);