CREATE TABLE IF NOT EXISTS `formation` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `degree` varchar(10) NOT NULL,
    `status` varchar(10) NOT NULL,
    `begindate` date NULL,
    `enddate` date NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `formation` (`id`, `name`, `degree`, `status`, `begindate`, `enddate`) VALUES
(1, 'Webdeveloper', 'BES', 'active', '2023-09-01', '2024-06-30'),
(2, 'Informatique de gestion', 'BAC', 'active', '2023-09-01', '2024-06-30');