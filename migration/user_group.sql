CREATE TABLE IF NOT EXISTS `user_group` (
     `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
     `userid` int(10) UNSIGNED NOT NULL,
     `groupid` bigint(20) UNSIGNED NOT NULL,
     `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`),
     UNIQUE KEY `userid` (`userid`,`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;