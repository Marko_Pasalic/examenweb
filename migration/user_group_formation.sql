CREATE TABLE IF NOT EXISTS `user_group_formation` (
     `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
     `user_group_id` int(10) UNSIGNED NOT NULL,
     `formationid` bigint(20) UNSIGNED NULL,
     `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`),
     UNIQUE KEY `user_group_id` (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;